<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Time extends Model
{
    protected $fillable = ["datetime","status","qty"];

    public function appointments()
    {
        return $this->hasMany('App\Models\Appointment','appointment_time','datetime');
    }
}
