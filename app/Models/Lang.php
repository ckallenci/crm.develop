<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;


class Lang extends Model
{
    use Cachable;

    public function getLangId($short)
    {
        $lang = Lang::where('short',$short)->first();

       return $lang;         
    }
}
