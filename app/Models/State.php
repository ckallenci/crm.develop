<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;


class State extends Model
{
    use Cachable;

    //
    public $timestamps = false;   

    public function cities()
    {
        return $this->hasMany('App\Models\City');
    }
    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }
}
