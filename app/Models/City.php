<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;


class City extends Model
{
    use Cachable;

    //
    public $timestamps = false;   

    public function state()
    {
        return $this->belongsTo('App\Models\State');
    }
}
