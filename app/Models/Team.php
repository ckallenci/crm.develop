<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    //
    protected $fillable = ["lang_id","page_id","image","first_name","last_name","department","email","phone","linkedin","googleplus",'skype','order','status'];

    public function contents()
    {
        return $this->morphMany('App\PageContent','page_contentable')->where('status',1)->orderBy('order');
    }
    public function contentsadmin()
    {
        return $this->morphMany('App\PageContent','page_contentable');
    }
}
