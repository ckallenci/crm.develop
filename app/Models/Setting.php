<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = ['lang_id', 'title', 'setting_type_id', 'setting_veriable', 'setting_value'];
    public $timestamps = false;
}
