<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $fillable = ["name","content","real_price","discount_price","total_use","status","order"];
}
