<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    //
    protected $fillable = ["lang_id","title_desc","title","text","button","order","status","image"];

}
