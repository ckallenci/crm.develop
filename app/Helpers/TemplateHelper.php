<?php

use App\Models\Template;

if (!function_exists('template_check')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function template_check()
    {
        $template = Template::where('status', 1)->first();
        return $template;
    }
}

if (!function_exists('template_path_check')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function template_path_check($page_path)
    {
        $template = Template::where('status', 1)->first();

        $files = File::exists(base_path().'/resources/views/'.$template->template_path.$page_path.'.blade.php');
        
        if($files)  // seçili temada böyle bir dosya var mı?
        {
            $layouts_extend = $template->template_path.$page_path;
        }
        else
        {
            $layouts_extend = $page_path;
        }
        
        return $layouts_extend;
    }
}
