<?php

namespace App\Http\Middleware;

use App\Models\Template;
use View;
use Closure;

class TemplateMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $temalar = Template::where('id', '>', 1)->where('status', 1)->first();

        if($temalar)
        {
            $secili_tema = Template::where('status', 1)->first();
            View::share('extend_uzanti', $secili_tema->template_path."/layouts.homeindex");
        }
        else
        {
            View::share('extend_uzanti', '/layouts.homeindex');
        }
    
        return $next($request);
    }
}
