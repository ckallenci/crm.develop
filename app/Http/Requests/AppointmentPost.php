<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AppointmentPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(\Auth::check()) {
            return true;
        }else{
            return true;
        }
        
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            "date"              => "required",
            "time"              => "required",
            "name"              => "required",
            "phone"             => "required",
            "note"              => "",
        ];
    }
    public function attributes(){

        return [
            "date"              => trans('site.appointmnent_date'),
            "time"              => trans('site.appointmnent_time'),
            "name"              => trans('site.appointmnent_name'),
            "phone"             => trans('site.appointmnent_phone'),
            "note"              => trans('site.appointmnent_note'),
        ];
    }
    
}
