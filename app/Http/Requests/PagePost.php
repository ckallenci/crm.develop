<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PagePost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            "page_id"           => 'required',
            "title"             => 'required',
            "meta_desc"         => 'required',
            "meta_keyword"      => 'required',
            "redirect_url"      => '',
            "page_type_id"     => 'required',
            "topmenu"           => '',
            "submenu"           => '',
            'page_img'          => 'image|mimes:jpeg,png,jpg,gif|max:2048',
        ];
    }
    public function attributes(){

        return [
            'title'             => 'Title',
            'meta_desc'         => 'Meta Des.',
            'meta_keyword'      => 'Meta Keyword',
            'redirect_url'      => 'Redirect URL',
            'page_type_id'      => 'Page Type',
            'topmenu'           => 'Top Menu',
            'submenu'           => 'Bottom Menu',
            'page_img'          => 'Image'
        ];

   }
}
