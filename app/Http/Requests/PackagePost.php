<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PackagePost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name"              => 'required',
            "content"           => 'required',
            "real_price"        => 'required',
            "discount_price"    => '',
            'total_use'         => 'required',
        ];
    }

    public function attributes(){

        return [
            'name'              => 'Package Name',
            'content'           => 'Content',
            'real_price'        => 'Price',
            'discount_price'    => 'Discount Price',
            'total_use'         => 'Total use',
        ];
    }
}
