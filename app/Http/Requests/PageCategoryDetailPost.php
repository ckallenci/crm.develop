<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PageCategoryDetailPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title"             => 'required',
            'image'             => 'required',
            'pdf'               => 'required',
            "page_id"           => 'required',
            "pagecategory_id"   => 'required',
        ];
    }
    public function attributes(){

        return [
            'title'             => 'Title',
            'image'             => 'Image',
            'pdf'               => 'PDF',
            'page_id'           => 'Page',
            'pagecategory_id'   => 'Page Category'
        ];
    }
}
