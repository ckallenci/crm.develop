<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SliderPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title_desc'        => '',
            "title"             => 'required',
            "text"              => '',
            "button_name[]"     => '',
            "button_url[]"      => '',
        ];
    }

    public function attributes(){

        return [
            'title_desc'        => 'Title Desc',
            'title'             => 'Title',
            'text'              => 'Text',
            'button_name[]'     => 'Button Name',
            'button_url[]'      => 'Button Url',
        ];
    }
}
