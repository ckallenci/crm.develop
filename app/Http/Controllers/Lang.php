<?php

namespace App\Http\Controllers;
use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class Lang extends Controller
{
    //
    public function index($lang)
    {
        Session::put('locale', $lang); 
        Session::save();
    
        \App::setLocale($lang);
        
        return redirect()->back();
    }
}
