<?php

namespace App\Http\Controllers\Admin;

use App\Models\Sponsor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Lang;
use App\Models\Page;
use View;
use Intervention\Image\ImageManager;
use Session;
use App\Http\Requests\SponsorPost;
use App\Http\Requests\SponsorImagePost;
use App\Models\Setting;



class SponsorController extends Controller
{
    protected $lang;
    protected $breadcrumb;
    protected $setting;

    public function __construct() 
    {
        // Fetch the Site Settings object
        $this->middleware('auth');

        $this->lang = Lang::where('short',\App::getLocale())->first();

        $this->breadcrumb[] =array('page' => "Home",'Link' => 'home');

        $this->setting = Setting::where('lang_id',$this->lang->id)->pluck('setting_value','setting_veriable')->all();


        View::share('breadcrumb', $this->breadcrumb);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page_id = (isset($request->page_id)) ? $request->page_id : 0;
        $page = Page::find($page_id);
        $sponsors = Sponsor::where('page_id',$page_id)->where('lang_id',$this->lang->id)->get();

        return view('dashboard.sponsor.index',compact('sponsors','page','page_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $page_id = (isset($request->page_id)) ? $request->page_id : 0 ;

        return view('dashboard.sponsor.create',compact('page_id'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SponsorPost $request)
    {
        //
        $result = $request->all();
        $result['lang_id'] = $this->lang->id;
        $result['status'] = 1; 
        $result['order'] = 100;

        $sponsor = new Sponsor;
        $sponsor->create($result);

        Session::flash('status', 1);

        return redirect()->route('sponsor.index',[null,'page_id'=>$result['page_id']]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Sponsor  $sponsor
     * @return \Illuminate\Http\Response
     */
    public function show(Sponsor $sponsor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Sponsor  $sponsor
     * @return \Illuminate\Http\Response
     */
    public function edit(Sponsor $sponsor,Request $request)
    {
        $page_id = (isset($request->page_id)) ? $request->page_id : 0 ;
        return view('dashboard.sponsor.edit',compact('page_id','sponsor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Sponsor  $sponsor
     * @return \Illuminate\Http\Response
     */
    public function update(SponsorPost $request, Sponsor $sponsor)
    {
        $result = $request->all();

        $sponsor->update($result);

        Session::flash('status', 1);

        return redirect()->route('sponsor.index',[null,'page_id'=>$sponsor->page_id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Sponsor  $sponsor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sponsor $sponsor)
    {
        //
        $page_id = $sponsor->page_id;
        $file= $sponsor->image;
        if($file!=""){
            $filename = public_path().$file;
            \File::delete($filename);
        }
        $sponsor->delete();

        return redirect()->route('sponsor.index',[null,'page_id'=>$page_id]);
    }

    public function image_upload(SponsorImagePost $request)
    {
        //dd($request->all());
        if ($request->file()) {
            $path = $request->file()['upload_image'];
            $coe =$path->getClientOriginalExtension();
            $manager = new ImageManager(array('driver' => 'gd'));
            $resize = $manager->make($path);
            if($this->setting['sponsor_image']!=""){
                $ex = explode('x',$this->setting['sponsor_image']);
                $x = ($ex[0]==0) ? null : $ex[0];
                $y = ($ex[1]==0) ? null : $ex[1];
                $resize->resize($x,$y, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
            }
            $resize->encode($coe);
            

            $hash = md5($resize->__toString().time());
            $path_url = "images/sponsor_img/{$hash}.".$coe;
            $resize->save(public_path($path_url));
            $url = "/" . $path_url;
            
            $url;
            return array('error'=>0 ,'url'=>$url);
        }else{
            return array('error'=>1,'message' => 'error');
        }
    }
    public function image_remove(Request $request)
    {
        //dd($request->img);
        $file= $request->img;
        $filename = public_path().$file;
        \File::delete($filename);
        return array('error'=>0 ,'url'=>'https://dummyimage.com/200x200/b3b1b3/000000.png&text=Image');
    }

    public function order(Request $request)
    {
        $page_id = (isset($request->page_id)) ? $request->page_id : 0 ;

        foreach ($request->order as $key => $value) {
            if (is_numeric($value)) {
                $sponsor = Sponsor::find($key);
                $sponsor->order = $value;
                $sponsor->update();
            }
        }
        Session::flash('status', 1);

        return redirect()->route('sponsor.index',[null,'page_id'=>$page_id]);
    }
    public function status(sponsor $sponsor)
    {
        if ($sponsor->status==1) {
            $sponsor->status=0;
        } else {
            $sponsor->status=1;
        }
        $page_id = $sponsor->page_id;
        $sponsor->update();
        Session::flash('status', 1);

        return redirect()->route('sponsor.index',[null,'page_id'=>$page_id]);
    }
}
