<?php

namespace App\Http\Controllers\Admin;

use App\Models\ContentFile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Lang;
use View;
use Intervention\Image\ImageManager;
use Session;
use App\Models\Setting;


class ContentFileController extends Controller
{
    protected $lang;
    protected $breadcrumb;
    protected $setting;

    public function __construct() 
    {
        $this->middleware('auth');

        $this->lang = Lang::where('short',\App::getLocale())->first();
        $this->setting = Setting::where('lang_id',$this->lang->id)->pluck('setting_value','setting_veriable')->all();

        $this->breadcrumb[] =array('page' => "Home",'Link' => 'dashboard');

        View::share('breadcrumb', $this->breadcrumb);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ContentFile  $contentFile
     * @return \Illuminate\Http\Response
     */
    public function show(ContentFile $contentFile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ContentFile  $contentFile
     * @return \Illuminate\Http\Response
     */
    public function edit(ContentFile $contentFile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ContentFile  $contentFile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ContentFile $contentFile)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ContentFile  $contentFile
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContentFile $contentFile)
    {
        //
    }

    public function image_upload(Request $request)
    {
        if ($request->file()) {
            
            $path = $request->file()['upload_image'];
            $coe =$path->getClientOriginalExtension();
            $minetype =$path->getMimeType();
            $manager = new ImageManager(array('driver' => 'gd'));
            $resize = $manager->make($path);
            if($this->setting['galery_1_image']!=""){
                $ex = explode('x',$this->setting['galery_1_image']);
                $x = ($ex[0]==0) ? null : $ex[0];
                $y = ($ex[1]==0) ? null : $ex[1];
                $resize->resize($x,$y,function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
            }
            $resize->encode($coe);
            $hash = md5($resize->__toString().time());
            $path_url = "images/content_file_img/{$hash}.".$coe;
            $resize->save(public_path($path_url));
            $url = "/" . $path_url;

            $manager = new ImageManager(array('driver' => 'gd'));
            $resize = $manager->make($path);
            if($this->setting['galery_1_thumbnail_image']!=""){
                $ex = explode('x',$this->setting['galery_1_thumbnail_image']);
                $x = ($ex[0]==0) ? null : $ex[0];
                $y = ($ex[1]==0) ? null : $ex[1];
                $resize->resize($x,$y,function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
            }
            $resize->encode($coe);
            $hash = md5($resize->__toString().time());
            $path_url = "images/content_file_img/t/{$hash}.".$coe;
            $resize->save(public_path($path_url));
            $thumbnail = "/" . $path_url;

            $contenfile = new ContentFile;
            $contenfile->page_content_id    = $request->pagecontent_id;
            $contenfile->file               = $url;
            $contenfile->file_thumbnail     = $thumbnail;
            $contenfile->type               = $minetype;
            $contenfile->save();
            
            $id = $contenfile->id;

            
            return array('error'=>0 ,'url'=>$thumbnail,'id'=>$id);
        }else{
            return array('error'=>1,'message' => 'error');
        }
    }
    public function image_remove(Request $request)
    {
        if ($request->id!="") {
            $contentfile = ContentFile::find($request->id);
           // dd($contentfile);
            $file = public_path().$contentfile->file;
            $file_thumbnail = public_path().$contentfile->file_thumbnail;
            \File::delete($file);
            \File::delete($file_thumbnail);
            $contentfile->delete();
            return array('error'=>0);
        }
       
    }

}
