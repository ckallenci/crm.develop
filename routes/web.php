<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/', "PageController@home")->name("index");


Route::get('{id}-{name}.html',"PageController@page")->name('a.page');
Route::get('{id}-{name}/{id2}-{name2}.html',"PageController@pagedetail")->name('a.pagedetail');
Route::post('{id}-{name}.html',"PageController@pagepost");

Route::resource('appointment','AppointmentController');
Route::resource('payment','PaymentController');

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('ajax/hour', 'AjaxController@hour')->name('hour');


Route::group(["middleware" => ["rolecheck:admin","auth"]], function () {
    
    Route::group(['namespace' => 'Admin'], function () {
        Route::resource('admin/page', 'PageController');
        Route::post('admin/page/order', 'PageController@order')->name('page.order');
        Route::get('admin/page/status/{page}', 'PageController@status')->name('page.status');

        Route::get('admin/page/{page}/home', 'PageController@home')->name('page.home');

        Route::get('admin/lang/{id}', 'Lang@index')->name('admin.lang');
        Route::resource('admin/pagecontent', 'PageContentController');
        Route::post('admin/pagecontent/order', 'PageContentController@order')->name('pagecontent.order');
        Route::get('admin/pagecontent/status/{pagecontent}', 'PageContentController@status')->name('pagecontent.status');
        Route::post('admin/pagecontetupload', 'PageContentController@contetupload')->name('contetupload.image');

        Route::resource('admin/team', 'TeamController');
        Route::post('image/team', 'TeamController@image_upload')->name('team.image');
        Route::put('image/team', 'TeamController@image_upload')->name('team.image');
        Route::post('imageremove/team', 'TeamController@image_remove')->name('team.imageremove');
        Route::post('admin/team/order', 'TeamController@order')->name('team.order');
        Route::get('admin/team/status/{team}', 'TeamController@status')->name('team.status');


        Route::resource('admin/sponsor', 'sponsorController');
        Route::post('image/sponsor', 'sponsorController@image_upload')->name('sponsor.image');
        Route::put('image/sponsor', 'sponsorController@image_upload')->name('sponsor.image');
        Route::post('imageremove/sponsor', 'sponsorController@image_remove')->name('sponsor.imageremove');
        Route::post('admin/sponsor/order', 'sponsorController@order')->name('sponsor.order');
        Route::get('admin/sponsor/status/{sponsor}', 'sponsorController@status')->name('sponsor.status');

        Route::resource('admin/setting', 'SettingController');
        Route::resource('admin/slider', 'SliderController');
        Route::post('admin/slider/order', 'SliderController@order')->name('slider.order');
        Route::get('admin/slider/status/{slider}', 'SliderController@status')->name('slider.status');

        Route::post('image/slider', 'SliderController@image_upload')->name('slider.image');

        Route::resource('admin/contentfile', 'ContentFileController');

        Route::post('image/contentfile', 'ContentFileController@image_upload')->name('contentfile.image');
        Route::put('image/contentfile', 'ContentFileController@image_upload')->name('contentfile.image');
        Route::post('imageremove/contentfile', 'ContentFileController@image_remove')->name('contentfile.imageremove');     

        Route::resource('admin/package', 'packageController');
        Route::post('admin/package/order', 'packageController@order')->name('package.order');
        Route::get('admin/package/status/{package}', 'packageController@status')->name('package.status');

        Route::resource('admin/time', 'TimeController');

    });
});

Route::get('lang/{id}', 'Lang@index')->name('lang');