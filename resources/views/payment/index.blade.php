@extends($extend_uzanti)

@section('content')
<div class="site-preloader">
    <div class="spinner">
        <div class="double-bounce1"></div>
        <div class="double-bounce2"></div>
    </div>
</div>
<div id="body-wrap">
    @include(template_path_check('/layouts/topmenu2'))
    <div class="container">
        <div class="row">
            <div class="offset-xl-2 col-xl-9 offset-lg-3 col-lg-6 offset-md-3 col-md-6">
                {!! Form::open(['route' => "appointment.store", 'method' => "POST", "class" =>"appointmnent", "id" =>"appointmnent"]) !!}
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="slidertitle">Payment</h1>
                    </div>
                    <div class="col-md-8">
                        {{ Form::dDateTime2('date', null, trans('site.appointmnent_date')) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::dSelect2('time', null, array() ,trans('site.appointmnent_time')) }}
                    </div>
                    <div class="col-md-12">
                        {{ Form::dText2('name',null , trans('site.appointmnent_name')) }}
                    </div>
                    <div class="col-md-12">
                        {{ Form::dText2('phone',null , trans('site.appointmnent_phone')) }}
                    </div>
                    <div class="col-md-12">
                        {{ Form::dTextarea2('note',null , trans('site.appointmnent_note')) }}
                    </div>
                    <div class="col-md-12 text-right">
                        {{ Form::dSubmit2('note',trans('site.appointmnent_btn')) }}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    
          

</div>
@endsection
