<section>
	@php
		if ($errors->has($name)) { $labelclass=["class" => "label state-error"]; } else { $labelclass=["class" => "label"]; }
	@endphp
	@if ($label_name!=null)
	{{ Form::label($name, $label_name, $labelclass) }}
	@endif
	<?php $t=array_merge(['id'=> $name,'class' => '','placeholder' => $placeholder],(array)$attributes);  ?>
	<div class="input {{ $errors->has($name) ? 'state-error' : '' }}">
	{{ Form::text($name, $value, $t) }}
	</div>
	@if ($errors->has($name))
		<div class="note note-error">
			{{ $errors -> first($name) }}
		</div>
	@endif
	
</section>
