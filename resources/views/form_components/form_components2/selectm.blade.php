<section>
		@php
			if ($errors->has($name)) { $labelclass=["class" => "label state-error"]; } else { $labelclass=["class" => "label"]; }
		@endphp
		@if ($label_name!=null)
		{{ Form::label($name, $label_name, $labelclass) }}
		@endif
		<div class="input {{ $errors->has($name) ? 'state-error' : '' }}">
			{{ Form::select($name, $list , $value, array_merge(['class' => 'select','placeholder' => $placeholder,'multiple'=>'multiple'])) }}
		</div>
		@if ($errors->has($name))
			<div class="note note-error">
				{{ $errors -> first($name) }}
			</div>
		@endif
	</section>