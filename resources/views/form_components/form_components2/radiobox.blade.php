<section>		
		@php
			if ($errors->has($name)) { $labelclass=["class" => "label state-error"]; } else { $labelclass=["class" => "label"]; }
		@endphp
		@if ($label_name!=null)
		{{ Form::label($name, $label_name, $labelclass) }}
		@endif
		<div class="inline-group">
			@foreach ($list as $item)
	
				<label class="radiobox">{{ Form::radio($name,$item['value'], Null,["class" => $name]) }}{{$item["text"]}}</label>
			@endforeach
	
			@if ($errors->has($name))
				<div class="note note-error">
					{{ $errors -> first($name) }}
				</div>
			@endif
		</div>
	</section>