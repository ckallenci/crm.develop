@if ($status==0)
    {{ Form::label($name, 'Okunmadı', ["class" => "control-label ",'style' => 'background-color:#2696c199; width:110px; height:30px; text-align:center; padding-top:5px']) }}
@endif
@if ($status==1)
    {{ Form::label($name, 'Cevaplandı', ["class" => "control-label ",'style' => 'background-color:#2f343e70; width:110px; height:30px; text-align:center; padding-top:5px; color: #fff;']) }}
@endif
@if ($status==2)
    {{ Form::label($name, 'Tamamlandı', ["class" => "control-label ",'style' => 'background-color:#2f343e70; width:110px; height:30px; text-align:center; padding-top:5px; color: #fff;']) }}
@endif
@if ($errors->has($name))
	<span class="help-block">
		<strong> {{ $errors -> first($name) }} </strong>
	</span>
@endif