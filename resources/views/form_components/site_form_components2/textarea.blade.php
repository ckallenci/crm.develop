<div class="form-group row {{ $errors->has($name) ? ' has-error' : '' }}">

    {{ Form::label($name, $label_name, ['class' => 'control-label col-md-4 col-form-label text-md-right']) }}
    
    <div class="col-md-8 text-md-left">
    
        {{ Form::textarea($name, $value, array_merge(['class' => 'form-control'], $attributes)) }}
    
        @if ($errors->has($name))
    
            <span class="help-block">
                <strong>{{ $errors->first($name) }}</strong>
            </span>
    
        @endif
    
    </div>
</div>