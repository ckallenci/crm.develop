<div class="form-group row {{ $errors->has($name) ? ' has-error' : '' }}">

    {{ Form::label($name, $label_name, ['class' => 'control-label col-md-4 col-form-label text-md-right']) }}
    
    <div class="col-md-8">

    <!--   <input id="mobile" type="tel" class="form-control{{ $errors->has('mobile') ? ' is-invalid' : '' }}" name="mobile" required onkeydown="
            return ( event.ctrlKey || event.altKey 
            || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
            || (95<event.keyCode && event.keyCode<106)
            || (event.keyCode==8) || (event.keyCode==9) 
            || (event.keyCode>34 && event.keyCode<40) 
            || (event.keyCode==46) )
            ">
    -->

        {{ Form::tel($name, $value, array_merge(['class' => 'form-control'], $attributes=[])) }}
    
        @if ($errors->has($name))
    
            <span class="help-block">
                <strong>{{ $errors->first($name) }}</strong>
            </span>
    
        @endif
    
    </div>
</div>