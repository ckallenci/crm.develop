<div class="form-group row {{ $errors->has($name) ? ' has-error' : '' }}">

    {{ Form::label($name, $label_name, ['class' => 'control-label col-md-4 col-form-label text-md-right']) }}
    
    <div class="col-md-8">
    
        {!! Form::select($name,$liste,$value, ['placeholder' => $placeholder, 'class' => 'form-control select',"data-width" =>"100%"]) !!}
    
        @if ($errors->has($name))
    
            <span class="help-block">
                <strong>{{ $errors->first($name) }}</strong>
            </span>
    
        @endif
    
    </div>
</div>