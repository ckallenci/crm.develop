@php
	if ($errors->has($name)) { $labelclass=["class" => "label state-error"]; $formerrror='has-error'; } else { $labelclass=["class" => "label"]; $formerrror=''; }
@endphp
<div class="form-group {{ $formerrror }}">
	@foreach ($list as $item)
		<div class="radio"><label>{{ Form::radio($name,$item['value'], Null,["class" => $name]) }}{{$item["text"]}}</label></div>
    @endforeach

	@if ($errors->has($name))
		<div class="help-block">
			{{ $errors -> first($name) }}
		</div>
	@endif
    
</div>