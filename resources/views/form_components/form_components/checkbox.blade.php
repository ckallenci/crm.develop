@php
	if ($errors->has($name)) { $labelclass=["class" => "label state-error"]; $formerrror='has-error'; } else { $labelclass=["class" => "label"]; $formerrror=''; }
@endphp
<div class="form-group {{ $formerrror }}">
	{{ Form::label($name, $label_name, $labelclass) }}
    @foreach ($list as $item)
        <div class="checkbox">
            <label>{{ Form::checkbox($name.'[]',$item['value'],$item['is_checked']) }}<i></i>{{$item["text"]}}</label>
        </div>
	@endforeach

	@if ($errors->has($name))
        <div class="help-block">
			{{ $errors -> first($name) }}
		</div>
	@endif
</div>
