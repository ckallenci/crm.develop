@php
	if ($errors->has($name)) { $labelclass=["class" => "label state-error"]; $formerrror='has-error'; } else { $labelclass=["class" => "label"]; $formerrror=''; }
@endphp
<div class="form-group {{ $formerrror }}">
	{{ Form::label($name, $label_name, $labelclass) }}
	<?php 
	$t=array_merge(['class' => 'form-control','placeholder' => $placeholder,'multiple'=>'multiple'],(array)$attributes);
	?>
	{{ Form::select($name, $list , $value, $t) }}
	@if ($errors->has($name))
		<div class="help-block">
			{{ $errors -> first($name) }}
		</div>
	@endif
</div>