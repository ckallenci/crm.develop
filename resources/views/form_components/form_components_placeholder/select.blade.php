<div class="form-group {{ $errors->has($name) ? ' has-error' : '' }}">

    {!! Form::select($name,$liste,$value, ['placeholder' => $placeholder, 'class' => 'form-control select',"data-width" =>"100%", 'id' => $id]) !!}
    @if ($errors->has($name))
        <span class="help-block">
            <strong>{{ $errors->first($name) }}</strong>
        </span>
    @endif
</div>