<div class="form-group row {{ $errors->has($name) ? ' has-error' : '' }}">
    <div class="col-md-12">
        {{ Form::tel($name, null, array_merge(['class' => 'form-control', 'placeholder'=> $placeholder], $attributes)) }}
        @if ($errors->has($name))
            <span class="help-block">
                <strong>{{ $errors->first($name) }}</strong>
            </span>
        @endif
    </div>
</div>