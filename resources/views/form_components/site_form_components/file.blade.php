@php
	if ($errors->has($name)) { $labelclass=["class" => "label state-error"]; } else { $labelclass=["class" => "label"]; }
@endphp
<section>
	{{ Form::label($name, $label_name, $labelclass) }}
	<div class="input input-file">
		
	<span class="button"><input type="file" id="{{ $name }}" name="{{ $name }}" onchange="this.parentNode.nextSibling.value = this.value">@lang('form.form_input_browse')</span><input type="text" placeholder="{{ $placeholder }}" readonly="">
		
		@if ($errors->has($name))
		<div class="note note-error">
				{{ $errors -> first($name) }}
			</div>
		@endif
	</div>
</section>