<div class="form-group {{ $errors->has($name) ? 'has-error' : '' }}" >
	{{ Form::label($name, $label_name, ["class" => "control-label col-md-3 col-sm-3 col-xs-12"]) }}
	<div class="col-md-9 col-sm-9 col-xs-12">
		<div class="input-group date datetimepiker">
		<?php $t=array_merge(['class' => 'form-control','placeholder' => $placeholder],(array)$attributes);  ?>
		{{ Form::text($name, $value, $t) }}
		<span class="input-group-addon">
                               <span class="glyphicon glyphicon-calendar"></span>
                            </span>
		@if ($errors->has($name))
			<span class="help-block">
				<strong> {{ $errors -> first($name) }} </strong>
			</span>
		@endif
		</div>
    </div>
</div>
