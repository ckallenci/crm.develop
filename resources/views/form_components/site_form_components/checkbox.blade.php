<section>		
	@php
		if ($errors->has($name)) { $labelclass=["class" => "label state-error"]; } else { $labelclass=["class" => "label"]; }
	@endphp
	{{ Form::label($name, $label_name, $labelclass) }}
	<div class="inline-group">
		@foreach ($list as $item)
			<label class="checkbox">{{ Form::checkbox($name.'[]',$item['value'],$item['is_checked']) }}<i></i>{{$item["text"]}}</label>
		@endforeach

		@if ($errors->has($name))
			<div class="note note-error">
				{{ $errors -> first($name) }}
			</div>
		@endif
    </div>
</section>