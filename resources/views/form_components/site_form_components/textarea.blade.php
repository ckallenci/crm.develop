<div class="form-group {{ $errors->has($name) ? 'has-error' : '' }}">
	@if ($label_name!=null)
	{{ Form::label($name, $label_name, ["class" => "control-label col-md-12 col-sm-12 col-xs-12",'style' => 'text-align: left;']) }}
		
	@endif
	
    	{{ Form::textarea($name, $value, array_merge(['class' => 'form-control','placeholder' => $placeholder])) }}
		@if ($errors->has($name))
			<span class="help-block">
				<strong> {{ $errors -> first($name) }} </strong>
			</span>
		@endif
	
</div>