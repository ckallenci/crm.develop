@extends('admin.layouts.app')

@section('content')
<div class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-12">
					<a class="btn btn-primary" href="{{ route('slider.create') }}" style="float: right;margin: 10px 0px;">Add</a>
				</div>
			</div>
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Sliders</h3>
				</div>
				<div class="box-body">
					{!! Form::open(['route' => ['slider.order'], 'method' => "POST",]) !!}

					<table class="table table-bordered">
						<thead>
							<tr>
								<th style="width: 50px;">Short NO</th>
								<th>Title</th>
                        		<th style="width: 10px;">Status</th>								
                        		<th>Action</th>
                        	</tr>
                      	</thead>

						@if ($sliders)
							<tbody>
								@foreach ($sliders as $slider)
									<tr>
											<td >{{ Form::dText2('order['.$slider->id.']','',null,$slider->order,['style'=> 'width: 50px;text-align: center;']) }}</td>
									
										<td>{{$slider->title_desc}} {{$slider->title}}</td>
										<td style="text-align: center;">
											<a href="{{ route('slider.status', ['id'=>$slider->id]) }}">@if ($slider->status==1) <i class="fa fa-eye" style="color:#356635; font-size: 19px;"></i> @else <i class="fa fa-eye-slash" style="color:red;font-size: 19px;"></i> @endif</a>
										</td>
										<td>
											<a href="{{ route('slider.edit', ['id'=>$slider->id]) }}" class="btn btn-primary action">Edit</a>
											<a href="{{ route('slider.destroy', ['id'=>$slider->id]) }}" data-method="delete" data-token="{{csrf_token()}}" data-confirm="Are you sure it will be deleted?" class="btn btn-danger action">Delete</a>
										</td>
									</tr>
								@endforeach
								<tr>
									<td>{{ Form::dSubmit('save','Short Save') }}</td>
									<td colspan="3"></td>
										
								</tr>
							</tbody>
						@endif
					</table>
				</div>
			</div>
		</div>
	</div>	
</div>
@endsection