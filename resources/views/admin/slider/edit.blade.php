@extends('layouts.app')

@section('content')
<div class="content">
		<section id="widget-grid" class="">
			<article class="col-sm-12 col-md-12 col-lg-6">
			<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false">
			<header>
				<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
				<h2>Slider Edit</h2>
			</header>
			<div>
				<div class="jarviswidget-editbox"></div>
					<div class="widget-body no-padding">
						{!! Form::model($slider, ['route' => ['slider.update' ,$slider->id], 'method' => "put", "class" =>"smart-form", 'files' => true]) !!}

						<fieldset>
							{{ Form::dText('title_desc','Title Desc.') }}
							{{ Form::dText('title','Title') }}
							{{ Form::dText('text','Text') }}
							
							@if ($slider->button)
								
								<?php
									$buttons = json_decode($slider->button);
								?>
								@foreach ($buttons as $button)
									<div class="col-md-6">
										{{ Form::dText('button_name[]','Button Name',Null,$button->button_name) }}
									</div>
									<div class="col-md-6">
										{{ Form::dText('button_url[]','Button Url',Null,$button->button_url) }}
									</div>
								@endforeach
								
							
							@else
								<div class="col-md-6">
								{{ Form::dText('button_name[]','Button Name',Null) }}
								</div>
								<div class="col-md-6">
								{{ Form::dText('button_url[]','Button Url',Null) }}
								</div>
							@endif
								
							
						<br/>

						</fieldset>	

						{{ Form::dSubmit('save','Update') }}
						{!! Form::close() !!}

				</div>
			</div>
		</div>
	</article>

	</section>
</div>

@endsection
