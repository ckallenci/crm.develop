@extends('admin.layouts.app')

@section('content')
<div class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-12">
                    <a class="btn btn-primary" href="{{ route('pagecontent.create',[Null,'type' => $type, 'oid'=>$oid]) }}" style="float: right;margin: 10px 0px;">Add</a>
                </a>
            </div>
        </div>
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Contents</h3>
            </div>
            <div class="box-body">
			    {!! Form::open(['route' => ['pagecontent.order', Null,  'type' => $type, 'oid'=>$oid], 'method' => "POST"]) !!}
					<table class="table table-bordered ">
						<thead>
							<tr>
								<th>Short NO</th>
								<th>Content</th>
                        		<th style="width: 10px;">Status</th>								
                        		<th style="width: 200px;">Action</th>
                        	</tr>
						  </thead>
						<?php $i=0; ?>
						@if ($contentlist)
							<tbody class="sortable-list">
								@foreach ($contentlist as $content)
									<tr style="width: 100%;" id="{{$content->id}}">
										<td style="width: 100px;">{{ Form::dText2('order['.$content->id.']','',null,$content->order,['class'=> 'btn'.$content->id,'style'=> 'width: 50px;text-align: center;']) }}</td>
										@if ($content->page_content_type_id == 1)
										<td id="content{{ $content->id }}">@if ($content->title)<h1>{{$content->title}}</h1>@endif<br/>@if ($content->html) {!! $content->html !!}@endif</td>
										@elseif($content->page_content_type_id == 2)
										<td id="content{{ $content->id }}">
											<div id="progress" class="progress"">
											<div class="progress-bar progress-bar-success"></div>
											</div>
											@if ($content->content_files)
												@foreach ($content->content_files as $file_item)
												<div class="file_image imageitem{{$file_item->id}}">
													<div class="removeimage " data-id="{{ $file_item->id }}">
														<i class="fa fa-trash-o"></i>
													</div>
													<img src="{{ $file_item->file_thumbnail }}" id="image_{{ $file_item->id }}" >
												</div>
												@endforeach
											@endif
										</td>
										@elseif($content->page_content_type_id == 3)
										<td id="content{{ $content->id }}">@if ($content->title)<h1>{{$content->title}}</h1>@endif<br/>@if ($content->html) {!! $content->html !!}@endif</td>
										@endif
										
										<td style="text-align: center;">
											<a href="{{ route('pagecontent.status', ['id'=>$content->id,'type' => $type,'oid'=>$oid] ) }}">@if ($content->status==1) <i class="fa fa-eye" style="color:#356635;    font-size: 19px;"></i> @else <i class="fa fa-eye-slash" style="color:red;font-size: 19px;"></i> @endif</a>
										</td>
										<td>
											@if ($content->page_content_type_id==2)
											
										<span class="imgup"><input type="file" id="upload_image" class="upload_image" name="upload_image" multiple data-id="{{ $content->id }}" onclick="imageupload({{ $content->id }})"><i class="fa fa-file"></i></span>
											@endif
											<a href="{{ route('pagecontent.edit', ['id'=>$content->id,'type' => $type,'oid'=>$oid]) }}" class="btn btn-primary action">Edit</a>
											<a href="{{ route('pagecontent.destroy', ['id'=>$content->id,'type' => $type,'oid'=>$oid]) }}" data-method="delete" data-token="{{csrf_token()}}" data-confirm="Are you sure it will be deleted?" class="btn btn-danger action">Delete</a>
											
										</td>
									</tr>
									<?php $i++; ?>
								@endforeach
								
							</tbody>
							@if ($i>0)
								
							
							<tfooter>
									<tr>
											<td>{{ Form::dSubmit('save','Short Save') }}</td>
											<td colspan="3"></td>
										</tr>
							</tfooter>
							@endif
						@endif
					</table>
					{!! Form::close() !!}
                </div>
			</div>
		</div>
	</div>	
</div>
@endsection
@section('css')
	<style>
		.imgup{
			display: inline-block;
    margin-bottom: 0;
    font-weight: 400;
    text-align: center;
    vertical-align: middle;
    touch-action: manipulation;
    cursor: pointer;
    background-image: none;
    border: 1px solid transparent;
    white-space: nowrap;
    padding: 6px 12px;
    font-size: 13px;
    line-height: 1.42857143;
    border-radius: 2px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
	color: #fff;
    background-color: #3276b1;
    border-color: #2c699d;
	position: relative;
		}
		.imgup input{
			position: absolute;
    top: 0;
    right: 0;
    padding: 0;
    font-size: 30px;
    cursor: pointer;
    opacity: 0;
		}
	</style>
<link rel="stylesheet" href="{{ asset('css/upload/jquery.fileupload.css') }}">
<style>
		.progress{
			height: 5px;
			margin-bottom: 0px;
		}
		.progress-bar-success{
			background-color: red;
		}
		section .file_image{
			width: 250px;
			position: relative;
			float: left;
		}
		section .file_image .removeimage{
			position: absolute;
			right: 5px;
			top: 2px;
			color: red;
			cursor: pointer;
		}
				
		section .file_image img{
			width: 100%;
		}
		</style>
@endsection

@section('js')
<script src="{{ asset('js/upload/vendor/jquery.ui.widget.js') }}"></script>
<script src="{{ asset('js/upload/jquery.iframe-transport.js') }}"></script>
<script src="{{ asset('js/upload/jquery.fileupload.js') }}"></script>
	<script>
		$(function () {
				'use strict';
				
				//imageupload();
				
			});
			function imageupload (id) {
				var url = "{{ route('contentfile.image',[null,'pagecontent_id'=>'']) }}"+id;
				var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
				$('.upload_image').fileupload({
					url: url,
					dataType: 'json',
					type: 'POST',
					
					headers: {
						'X-CSRF-TOKEN': CSRF_TOKEN,
					},
					done: function (e, data) {
						if (data.result.error==0) {
							var file = data.result;
							var datid = file.id;
							var html = '<div class="file_image imageitem'+datid+'">';
							html+='<div class="removeimage" data-id="'+datid+'">';
							html+='<i class="fa fa-trash-o"></i>';
							html+='</div>';
							html+='<img src="'+file.url+'" id="image_'+datid+'" >';
							html+='</div>';
							$('#content'+id).append(html);
							imgremovefun()

						}else{
							alert(data.message);
						}
					},
					progressall: function (e, data) {
						var progress = parseInt(data.loaded / data.total * 100, 10);
            			$('#progress .progress-bar').css(
                			'width',
                			progress + '%'
            			);
											
					}
				}).prop('disabled', !$.support.fileInput)
        			.parent().addClass($.support.fileInput ? undefined : 'disabled');
			}
			$(function () {
				imgremovefun()
			});
			function imgremovefun() {
				$('.removeimage').on('click',function() {
					var id = $(this).data('id');

					
					$.ajax({
						method: "POST",
						url: "{{ route('contentfile.imageremove') }}",
						data: { id: id },
						headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						}
					})
					.done(function( data ) {
						if (data.error==0) {
							$('.imageitem'+id).remove();
						}else{
							alert(data.message);
						}
						
						
					});
				});
			}
	</script>
@endsection
{{-- @section('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection
@section('js')
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
/*$( function() {
	$('.sortable-list').sortable({ 
	connectWith: '.sortable-list',
	update: function(event, ui) {
		var changedList = this.id;
		var order = $(this).sortable('toArray');
		for (let i = 0; i < order.length; i++) {
			console.log(order[i]);
			$('.btn'+order[i]).attr('value',(i*10)+10);
		}
	}
	});
} );*/
</script>
@endsection --}}
