@extends('layouts.app')

@section('content')
<div class="content">
		<section id="widget-grid" class="">
			<article class="col-sm-12 col-md-12 col-lg-6">
			<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false">
			<header>
				<span class="widget-icon"> <i class="fa fa-add"></i> </span>
				<h2>Sponsor Add</h2>
			</header>
			<div>
				<div class="jarviswidget-editbox"></div>
					<div class="widget-body no-padding">
						{!! Form::open(['url' => 'admin/sponsor?page_id='.$page_id, 'method' => "POST", "class" =>"smart-form", 'files' => true]) !!}

						<fieldset>
							{{ Form::dText('name','Name') }}
							{{ Form::dFile2('image','Image') }}
							{{ Form::dText('url','URL') }}
							{{ Form::hidden('page_id', $page_id) }}
						<br/>

						</fieldset>	
							{{ Form::dSubmit('save','Insert') }}
						<br/>
						{!! Form::close() !!}


					</div>
			</div>
		</div>
	</article>

	</section>
</div>


@endsection
@section('css')
<link rel="stylesheet" href="{{ asset('css/upload/jquery.fileupload.css') }}">
@endsection
@section('js')
<script src="{{ asset('js/upload/vendor/jquery.ui.widget.js') }}"></script>
<script src="{{ asset('js/upload/jquery.iframe-transport.js') }}"></script>
<script src="{{ asset('js/upload/jquery.fileupload.js') }}"></script>
	<script >
		
			$(function () {
				'use strict';
				
				imageupload();
				$('#image_image').on('click',function() {
					$('#upload_image').trigger('click');
				});
				var i = $('#image').val();
				if (i!="") {
					$('#image_image').attr('src',i);
				}
			});
			function imageupload () {
				var url = "{{ route('sponsor.image') }}";
				$('#upload_image').fileupload({
					url: url,
					dataType: 'json',
					method: "POST",
					headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						},
					done: function (e, data) {
						if (data.result.error==0) {
							var file = data.result;
							
							$('#image_image').attr('src',file.url);
							$('#image').val(file.url);

						}else if(data.result.errors!=null){
							alert('1 => '+data.result.message);
						}else{
							alert('2 => '+data.result.error);
						}
					},
					progressall: function (e, data) {
						var progress = parseInt(data.loaded / data.total * 100, 10);
            			$('#progress .progress-bar').css(
                			'width',
                			progress + '%'
            			);
											
					}
				}).prop('disabled', !$.support.fileInput)
        			.parent().addClass($.support.fileInput ? undefined : 'disabled');
			}
			$(function () {
				$('.removeimage').on('click',function() {
					var imgpath = $('#image').val();
					
					$.ajax({
						method: "POST",
						url: "{{ route('sponsor.imageremove') }}",
						data: { img: imgpath },
						headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						}
					})
					.done(function( data ) {
						$('#image_image').attr('src',data.url);
						$('#image').val('');
					});
				});
			});
			
	</script>
@endsection
