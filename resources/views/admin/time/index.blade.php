@extends('admin.layouts.app')

@section('content')
<div class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-12">
					<a class="btn btn-primary" href="{{ route('package.create') }}" style="float: right;margin: 10px 0px;">Add</a>
				</div>
			</div>
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Time
					</h3>
				</div>
				<div class="box-body">
					{!! Form::open(['route' => ['time.store'], 'method' => "POST"]) !!}
					<button type="submit" value="" class="btn btn-success">Kaydet</button>
					
					<div class="jcarousel-wrapper">
						<div class="jcarousel button-group-pills">
							
							<ul>
									
								@php
									 $i = $startdate
								@endphp
								@while ($i < $finishdate)
									
									<li>
										<p class="listdate">{{ date('d.m.Y',$i) }}</p>
										@foreach ($interval as $intervalitem)
										<div class="row">
											<div class="col-md-6">
												<label class="btn btn-default" id="{{ $i+$intervalitem }}">
													<input type="checkbox" name="time[]" value="{{ $i+$intervalitem }}" @if (in_array($i+$intervalitem,$dates)) checked="checked" @endif>
													<div>{{ date('H:i',$intervalitem) }}</div>
												</label>
											</div>
											<div class="col-md-6">
												<input type="text" class="qty" name="qty[]" id="qty{{ $i+$intervalitem }}" value="@if (isset($qtys[$i+$intervalitem])) {{ $qtys[$i+$intervalitem] }} @endif">
											</div>
										</div>
										@endforeach
									</li>
									<?php $i = $i+24*60*60; ?>
								@endwhile
								
									
							</ul>
						</div>
			
						<a href="#" class="jcarousel-control-prev">&lsaquo;</a>
						<a href="#" class="jcarousel-control-next">&rsaquo;</a>
					</div>
					{!! Form::close() !!}

					</div>
				</div>
			</div>
		</div>
	</article>
	
</div>
@endsection
@section('css')
<style>
	.listdate{
		display: block;
		text-align: center;
		font-weight: bold;
		font-family: 'Roboto',sans-serif;
		font-size: 16px;
	}
	.button-group-pills .btn input{
		position: absolute;
		display: none;
	}
	.qty{
		width: 50%;
		text-align: center;
	}
	.jcarousel-control-prev{
		left: -25px;
	}
	.jcarousel-control-next{
		right: -25px;
	}
</style>
	
@endsection
@section('js')
	<script>
		$(function() {
        var jcarousel = $('.jcarousel');

        jcarousel
            .on('jcarousel:reload jcarousel:create', function () {
                var carousel = $(this),
                    width = carousel.innerWidth();

                if (width >= 600) {
                    width = width / 7;
                } else if (width >= 350) {
                    width = width / 5;
                }

                carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');
            })
            .jcarousel({
                wrap: 'circular'
            });

        $('.jcarousel-control-prev')
            .jcarouselControl({
                target: '-=1'
            });

        $('.jcarousel-control-next')
            .jcarouselControl({
                target: '+=1'
            });

        $('.jcarousel-pagination')
            .on('jcarouselpagination:active', 'a', function() {
                $(this).addClass('active');
            })
            .on('jcarouselpagination:inactive', 'a', function() {
                $(this).removeClass('active');
            })
            .on('click', function(e) {
                e.preventDefault();
            })
            .jcarouselPagination({
                perPage: 1,
                item: function(page) {
                    return '<a href="#' + page + '">' + page + '</a>';
                }
            });
		
		
	
	});
	$(function() {
		
		$('.button-group-pills .btn').on('click',function() {
			checkcheck();
		});
	})
	function checkcheck() {
		$('.button-group-pills .btn').removeClass('active');
		$('.button-group-pills input:checked').each(function() {
			var id = $(this).val();
			$('#'+id).addClass('active');
		});
	}
	checkcheck();
	</script>
@endsection