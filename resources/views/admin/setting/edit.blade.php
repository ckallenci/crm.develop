@extends('layouts.app')

@section('content')
<div class="content">
		<section id="widget-grid" class="">
			<article class="col-sm-12 col-md-12 col-lg-6">
			<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false">
			<header>
				<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
				<h2>Setgin Edit</h2>
			</header>
			<div>
				<div class="jarviswidget-editbox"></div>
					<div class="widget-body no-padding">
						{!! Form::model($setting, ['route' => ['setting.update' ,$setting->id], 'method' => "put", "class" =>"smart-form"]) !!}

						<fieldset>
							{{ Form::dText('title','Title') }}
							@if ($setting->setting_type_id == 1)
								{{ Form::dText('setting_value','Setting Value') }}
							@elseif($setting->setting_type_id == 2)
								{{ Form::dTextarea('setting_value','Setting Value') }}
							@endif

						</fieldset>	

						{{ Form::dSubmit('save','Update') }}
						{!! Form::close() !!}

				</div>
			</div>
		</div>
	</article>

	</section>
</div>

@endsection
