@extends('layouts.app')

@section('content')
<div class="content">
		<section id="widget-grid" class="">
			<article class="col-sm-12 col-md-12 col-lg-6">
			<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false">
			<header>
				<span class="widget-icon"> <i class="fa fa-add"></i> </span>
				<h2>Page Add</h2>
			</header>
			<div>
				<div class="jarviswidget-editbox"></div>
					<div class="widget-body no-padding">
						{!! Form::open(['url' => 'admin/page', 'method' => "POST", "class" =>"smart-form", 'files' => true]) !!}

						<fieldset>
							{{ Form::dText('title','Title') }}
							{{ Form::dText('meta_desc','Meta Des.') }}
							{{ Form::dText('meta_keyword','Meta Keyword') }}
							{{ Form::dText('redirect_url','Redirect URL') }}
							{{ Form::dSelect('page_type_id','Page Type',$pagetypes) }}
							{{ Form::dCheckboxone('topmenu','Top Menu',1,null) }}
							{{ Form::dCheckboxone('submenu','Bottom Menu',1,null) }}
							{{ Form::hidden('page_id', $page_id) }}
							{{ Form::dFile('page_img','Image') }}
						<br/>

						</fieldset>	
							{{ Form::dSubmit('save','Insert') }}
						<br/>
						{!! Form::close() !!}


					</div>
			</div>
		</div>
	</article>

	</section>
</div>


@endsection

