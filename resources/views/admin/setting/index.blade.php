@extends('layouts.app')

@section('content')
<div class="content">
<section id="widget-grid" class="">
    
    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    	<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false">
			<header>
				<span class="widget-icon"> <i class="fa fa-table"></i> </span>
				<h2>Settings</h2>
			</header>
			<div>
				<div class="jarviswidget-editbox">
				</div>
				
				<div class="widget-body">
					
					<div class="table-responsive">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>Setting Title</th>
								<th>Setting Veriable</th>
                        		<th>Action</th>
                        	</tr>
                      	</thead>

						@if ($settings)
							<tbody>
								@foreach ($settings as $setting)
									<tr>
										<td style="width: 200px;"> {{ $setting->title }}</td>
										
										<td>{{$setting->setting_veriable}}</td>
										<td style="width: 100px;">
											<a href="{{ route('setting.edit', ['id'=>$setting->id]) }}" class="btn btn-primary action">Edit</a>
										</td>
									</tr>
								@endforeach
							</tbody>
						@endif
					</table>
					</div>
				</div>
			</div>
		</div>
	</article>
	
</div>
@endsection