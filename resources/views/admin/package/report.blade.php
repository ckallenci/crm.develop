@extends('layouts.master')

@section('content')
<div class="right_col" role="main">
<div class="container">
    <div class="row">
    	<div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
            	<div class="x_title">
            		 <h2>Siteler</h2>
            		 <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>

            	</div>
      {!! Form::open(['route' => ['site.report'], 'method' => "get",]) !!}
            <?php 
            $totaltraffic =0;
            $totalunique_traffic =0;
            
             ?> 
              <div class="col-sm-12">
                <div class="row">
                      <?php $tlist = array();
                      $tp=array();
                      $tpu=array();
          foreach ($devices as $key => $element){

            $tlist[] = array('value' => $key,'text' => $element,'is_checked' =>false);
          } ?>
                      <div class="col-sm-3">{{ Form::dCheckbox2('device','Cihaz',$tlist) }}</div>
                      <div class="col-sm-3">{{ Form::dSelectm2('tags[]','Etiket',$tags) }}</div>
                      <div class="col-sm-3">  {{ Form::dSubmit('save','Ara') }}</div>
                  </div>
              </div>
            	<div class="x_content">
            		<table id="datatable-buttons" class="table table-striped table-bordered">
            			<thead>
                        <tr>
                          <th rowspan="2">Site Adı</th>
                          <th rowspan="2">Trafik</th>
                          @if ($codes)
                            @foreach ($codes as $item)
                                <?php 
                                $tp[format_slug($item->name)] =0; 
                                $tpu[format_slug($item->name)] =0; 
                                ?>
                              <th colspan="2" style="text-align: center;">{{ $item->name }}</th>
                            @endforeach
                          @endif
                        </tr>
                        <tr>
                           @if ($codes)
                            @foreach ($codes as $item)
                            <th>CPM</th>
                            <th>CPC</th>
                            @endforeach
                          @endif
                        </tr>
                      </thead>

					@if ($sites)
					<tbody>
						@foreach ($sites as $site)
						<tr>
                        <td>{{$site->site_name}}</td>
						<td>{{$site->traffic.' / '. $site->unique_traffic}}</td>
                        <?php 
                        $totaltraffic+=$site->traffic;
                        $totalunique_traffic+=$site->unique_traffic;
                        
                         ?>
              @if ($codes)
                @foreach ($codes as $item)
                
                    <?php 
                    $cpm = $site->site_prices->where('code_id',$item->id)->first()['cpm_price'];
                    $cpc = $site->site_prices->where('code_id',$item->id)->first()['cpc_price'];
                    if ($site->traffic>0 && $cpm>0) {
                        $tp[format_slug($item->name)] +=( ($site->traffic/1000)*$cpm ); 
                        $tpu[format_slug($item->name)] =( ($site->unique_traffic/1000)*$cpm ); 
                    }
                    ?>
                   
                  <td>{{ number_format($cpm,2) }}</td>
                  <td>{{ number_format($cpc,2) }}</td>
                  
                @endforeach
              @endif
							
						</tr>
						@endforeach
                        <tr>
                            <td colspan="2" style="text-align: right;">Trafik ortalam / Unique ortalama: {{$totaltraffic.' / '.$totalunique_traffic}} </td>
                            @if ($codes)
                            @foreach ($codes as $item)
                                @if ($totaltraffic>0)
                                <td>{{ number_format(($tp[format_slug($item->name)]/($totaltraffic/1000)),2) }}</td>
                                <td>{{ number_format(($tpu[format_slug($item->name)]/($totalunique_traffic/1000)),2) }}</td>
                                @else
                                <td>{{ number_format(0,2) }}</td>
                                <td>{{ number_format(0,2) }}</td>
                                @endif
                                
                            @endforeach
                            @endif
                        </tr>
					</tbody>
                	@endif
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
@endsection
@section('js')
  <script type="text/javascript">
    $('.selectize').selectize({
      //createOnBlur: true,
        create: true,
        delimiter: ',',
        persist: false,
        plugins: ['remove_button'],

        });
  </script>
@endsection
<?php function remove_accents($string)
{
    $a = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
    $b = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
    $string = strtr(utf8_decode($string), utf8_decode($a), $b);
    return utf8_encode($string);
}

function format_slug($title)
{
    $title = remove_accents($title);
    $title = trim(strtolower($title));
    $title = preg_replace('#[^a-z0-9\\-/]#i', '_', $title);
    return trim(preg_replace('/-+/', '-', $title), '-/');
} ?>