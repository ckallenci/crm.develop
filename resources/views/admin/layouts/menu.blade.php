'<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
            <i class="fa fa-user"></i>
        </div>
        <div class="pull-left info">
          <p>{{ env('APP_NAME') }}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        
        <li>
          <a href="{{ route('page.index') }}">
            <i class="fa fa-th"></i> <span>Pages</span>
          </a>
        </li>
        <li>
          <a href="{{ route('slider.index') }}">
            <i class="fa fa-pie-chart"></i>
            <span>Slider</span>
          </a>
        </li>
        <li>
          <a href="{{ route('setting.index') }}">
            <i class="fa fa-calendar"></i> <span>Setting</span>
          </a>
        </li>
        <li>
          <a href="{{ url('admin/member-list') }}">
            <i class="fa fa-envelope"></i> <span>Mailbox</span>
          </a>
        </li>
        <li>
            <a href="{{ route('package.index') }}">
                <i class="fa fa-box"></i> <span>Packege</span>
            </a>
        </li>  
        <li>
            <a href="{{ route('time.index') }}">
                <i class="fa fa-clock-o"></i> <span>Time</span>
            </a>
        </li>        
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>