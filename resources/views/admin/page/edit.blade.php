@extends('admin.layouts.app')

@section('content')
{!! Form::model($page, ['route' => ['page.update' ,$page->id,  'page_id' => $page->page_id ], 'method' => "put", "class" =>"smart-form", 'files' => true,'role'=>'form']) !!}
		<div class="box-body">
<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title">Page Edit</h3>
	</div>
	
			{{ Form::dText('title','Title') }}
			{{ Form::dText('meta_desc','Meta Des.') }}
			{{ Form::dText('meta_keyword','Meta Keyword') }}
			{{ Form::dText('redirect_url','Redirect URL') }}
			{{ Form::dSelect('page_type_id','Page Type',$pagetypes) }}
			{{ Form::dCheckboxone('topmenu','Top Menu',1,null) }}
			{{ Form::dCheckboxone('submenu','Bottom Menu',1,null) }}
			{{ Form::hidden('page_id') }}
			{{ Form::dFile('page_img','Image') }}
		</div>
		<div class="box-footer">
			{{ Form::dSubmit('save','Update') }}
		</div>
	
</div>
{!! Form::close() !!}
@endsection
