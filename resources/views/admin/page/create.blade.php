@extends('admin.layouts.app')

@section('content')
<div class="box box-primary">
	<div class="box-header with-border">
	  <h3 class="box-title">Page Add</h3>
	</div>
	
	{!! Form::open(['url' => 'admin/page', 'method' => "POST", "class" =>"smart-form", 'files' => true,'role'=>'form']) !!}

		<div class="box-body">
			{{ Form::dText('title','Title') }}
			{{ Form::dText('meta_desc','Meta Des.') }}
			{{ Form::dText('meta_keyword','Meta Keyword') }}
			{{ Form::dText('redirect_url','Redirect URL') }}
			{{ Form::dSelect('page_type_id','Page Type',$pagetypes) }}
			{{ Form::dCheckboxone('topmenu','Top Menu',1,null) }}
			{{ Form::dCheckboxone('submenu','Bottom Menu',1,null) }}
			{{ Form::hidden('page_id', $page_id) }}
			{{ Form::dFile('page_img','Image') }}
		</div>
		<div class="box-footer">
			{{ Form::dSubmit('save','Insert') }}
		</div>
	{!! Form::close() !!}
</div>

@endsection

