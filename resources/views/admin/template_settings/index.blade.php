@extends('layouts.app')

@section('content')
<div class="content">
    <section id="widget-grid" class="">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false">
                <header><h2>Template Settings</h2></header>
                <div>
                    <div class="jarviswidget-editbox"></div>
                    <div class="widget-body">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th> Status </th>
                                    <th> Template Name </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($templates as $template)
                                    <tr>
                                        <td> 
                                            <input type="radio" name="status" class="durum" data-id="{{$template->id}}", data-url="status_change" {{$template->status ? "checked" : null}}>  
                                        </td>
                                        <td> {{ $template->template_name }} </td>  
                                    </tr>     
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </article>
    </section>
</div>

@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('admin/css/bootstrap-switch.min.css') }}">
@endsection

@section('js')
    <script src="{{ asset('admin/js/bootstrap-switch.min.js') }}"></script>
    <script>        
        $(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            var durum = parseInt($("body").data("status"));
            $('[data-toggle="tooltip"]').tooltip();
            $(".durum").bootstrapSwitch();
            $(".durum").on('switchChange.bootstrapSwitch', function(event, state) {
                $.ajax({
                    data: {"durum": state,"id":$(this).data("id") },
                    type: "POST",
                    url: $(this).data("url"),
                    success: function(url) {
                        //alert('Success');
                    }
                });
            });
        })
    </script>
@endsection