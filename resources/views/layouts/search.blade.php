@if ($tourpage)
    

<form action="{{ route('a.page',['name'=>str_slug($tourpage->title),'id'=>$tourpage->id]) }}" class="back-ground-image" method="GET">
    <div class="search-box">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-4">
                    {{ Form::sText('tour_name_like',trans('site.tour_name_like')) }}
                </div>
                <div class="col-md-2">
                    {{ Form::sSelect('continent_id',trans('site.continent'),$tourfilter['contiment_id'],'') }}
                </div>
                <div class="col-md-2">
                    {{ Form::sSelect('country_id',trans('site.country'),$tourfilter['countries'],'') }}
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-2"></div>
                <div class="col-md-2">
                    {{ Form::sSelect('style_id',trans('site.style_id'),[],trans('site.search_style_selected')) }} 
                </div>
                <div class="col-md-2">
                    {{ Form::sSelect('duration',trans('site.duration'),$tourfilter['duration'],'') }}
                </div>
                <div class="col-md-2">
                    {{ Form::sSelect('budget',trans('site.budget'),$tourfilter['budget'],'') }}
                </div>
                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-12">
                    <button type="submit" class="btn btn-primary search_tour_button"><i class="fas fa-search-location"></i> {{ trans('site.search') }} </button>
                </div>
            </div>
        </div>
    </div>
</form>

@endif

@push('scripts')
    <script>
        $( document ).ready(function() {
            $('#country_id').on('change',function() {
                var country_id = $('#country_id').val();
                $.ajax({
                    type: "GET",
                    url: "{{ route('ajax.cities_subcategories_by_country') }}",
                    data:'country_id='+country_id,
                    success: function(data){
                        $('#style_id').empty();
                        $('#style_id').append($('<option>').text('').attr('value', ""));
                        $.each(data, function(i, value) {
                            $('#style_id').append($('<option>').text(value.Name).attr('value', value.ID));
                        });
                    }
                });
            });
        });
    </script>
@endpush