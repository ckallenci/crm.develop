<section class="breadcrumbs extra">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    @if (isset($breadcrumblist))
                        
                        <h2>{{ $breadcrumblist[(count($breadcrumblist)-1)]['name'] }}</h2>
                        <ul>
                        
                            <li><a href="{{ route('index') }}">{{ $homepage->title }}</a></li>
                            @if (isset($page))
                                @if ($page->toppages)
                                    <li><i class="fa fa-angle-double-right"></i></li>
                                    <li><a class="" href="{{ route('a.page',['name'=>str_slug($page->toppages->title),'id' => $page->toppages->id]) }}">{{ $page->toppages->title }}</a></li>
                                @endif
                            @endif
                        @foreach ($breadcrumblist as $blistitem)
                            <li><i class="fa fa-angle-double-right"></i></li>
                            <li><a class="" href="{{ $blistitem['url'] }}">{{ $blistitem['name'] }}</a></li>
                        @endforeach
                    @else
                        @if (isset($breadcrumb))
                        <h2>{{ $breadcrumb->title }}</h2>
                        @else
                            @if (isset($page))
                                <h2>{{ $page->title }}</h2>
                            @endif
                        @endif
                        
                        <ul>
                        
                            <li><a href="{{ route('index') }}">{{ $homepage->title }}</a></li>
                            @if (isset($page))
                                @if ($page->toppages)
                                    <li><i class="fa fa-angle-double-right"></i></li>
                                    <li><a class="" href="{{ route('a.page',['name'=>str_slug($page->toppages->title),'id' => $page->toppages->id]) }}">{{ $page->toppages->title }}</a></li>
                                @endif
                            @endif
                            @if (isset($page))
                            <li><i class="fa fa-angle-double-right"></i></li>
                            <li><a @if (!isset($breadcrumb)) class="active" @endif href="{{ route('a.page',['name'=>str_slug($page->title),'id' => $page->id]) }}">{{ $page->title }}</a></li>
                            @endif
                            
                            @if (isset($breadcrumb))
                                <li><i class="fa fa-angle-double-right"></i></li>
                                <li><a class="active" href="{{ route('a.pagedetail',['name'=>str_slug($page->title),'id' => $page->id,'name2'=>str_slug($breadcrumb->title),'id2' => $breadcrumb->id]) }}">{{ $breadcrumb->title }}</a></li>
                            @endif
                            

                        </ul>
                    @endif
                    
                </div>
            </div>
        </div>
    </section>