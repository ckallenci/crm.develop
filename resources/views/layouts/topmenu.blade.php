<header id="mainHeader" class="header">
    <!-- Start Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light p-0 ">
        <div class="container-fluid">
           <div class="row align-items-start hundred-percent">
                <div class="col-md-3">
                    <a class="navbar-brand" href="{{ route('index') }}">
                        <img class="logo-dark" src="{{ asset('images/logo.png') }} " alt="">
                    </a>
                </div>
                <div class="col-md-9 align-top">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <i class="fas fa-bars"></i>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto">
                            @if ($topmenus)
                                @foreach ($topmenus as $pageitem)
                                    
                                    @if (count($pageitem->subpages)>0)
                                        <li class="nav-item dropdown @if (in_array($pageitem->id,$activemenu)) active @endif">
                                        <a class="nav-link dropdown-toggle"  href="#" id="down{{ str_slug($pageitem->title) }}" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ $pageitem->title }}</a>

                                        <div class="dropdown-menu" aria-labelledby="down{{ str_slug($pageitem->title) }}">
                                            @foreach ($pageitem->subpages as $subpage)
                                            <a class="dropdown-item" href="{{ route('a.page',['name'=>str_slug($subpage->title),'id'=>$subpage->id]) }}">{{$subpage->title}}</a>
                                            @endforeach
                                        </div>                                    
                                    @else
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('a.page',['name' => str_slug($pageitem->title),'id'=>$pageitem->id]) }}">{{ $pageitem->title }}</a>
                                    @endif
                                    </li>
                                @endforeach
                            @endif
                        
                        
                                @if ($langs)
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle"  href="#" id="downlang" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="flag-icon iti-flag {{ str_replace('flag-','',$lang->flag) }}"></i> {{ $lang->name }}</a>
                                        <div class="dropdown-menu" aria-labelledby="downlang">
                                        @foreach ($langs as $item)
                                            @if ($lang->id != $item->id)
                                            <a class="dropdown-item" href="{{route('lang',['id' => $item->short])}}"> <i class="flag-icon iti-flag {{ str_replace('flag-','',$item->flag) }}"></i> {{ $item->name }}</a>
                                            @endif
                                            
                                        @endforeach
                                        </div>
                                </li>                        
                                @endif
                            
                                
                        </ul>
                    {{--  {{
                        <a href="pricingTable.html" class="mr_btn_fill">Buy Tickets</a>
                        }} --}}
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <!-- End Navigation -->
    <div class="clearfix"></div>
</header>