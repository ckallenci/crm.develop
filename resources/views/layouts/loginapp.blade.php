<!DOCTYPE html>
<html lang="en-us" id="extr-page">
	<head>
		<meta charset="utf-8">
        <meta name="description" content="">
		<meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        
        <title>{{ config('app.name', 'Travelshop Admin') }}</title>
        
		
		<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('admin/css/bootstrap.min.css') }}">
		<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('admin/css/font-awesome.min.css') }}">
		<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('admin/css/smartadmin-production-plugins.min.css') }}">
		<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('admin/css/smartadmin-production.min.css') }}">
		<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('admin/css/smartadmin-skins.min.css') }}">
		<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('admin/css/smartadmin-rtl.min.css') }}"> 
		<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('admin/css/demo.min.css') }}">
		<link rel="shortcut icon" href="{{ asset('admin/img/favicon/favicon.ico') }}" type="image/x-icon">
		<link rel="icon" href="{{ asset('admin/img/favicon/favicon.ico') }}" type="image/x-icon">
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">
		<link rel="apple-touch-icon" href="{{ asset('admin/img/splash/sptouch-icon-iphone.png') }}">
		<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('admin/img/splash/touch-icon-ipad.png') }}">
		<link rel="apple-touch-icon" sizes="120x120" href="{{ asset('admin/img/splash/touch-icon-iphone-retina.png') }}">
		<link rel="apple-touch-icon" sizes="152x152" href="{{ asset('admin/img/splash/touch-icon-ipad-retina.png') }}">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<link rel="apple-touch-startup-image" href="{{ asset('admin/img/splash/ipad-landscape.png') }}" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
		<link rel="apple-touch-startup-image" href="{{ asset('admin/img/splash/ipad-portrait.png') }}" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
        <link rel="apple-touch-startup-image" href="{{ asset('admin/img/splash/iphone.png') }}" media="screen and (max-device-width: 320px)">
    </head>
    <body class="animated fadeInDown">

        <div id="main" role="main">

            <div id="content" class="container">
                @yield('content')
            </div>
        </div>    
        @yield('js')
    </body>
</html>