@extends($extend_uzanti)

@section('content')
<div class="site-preloader">
    <div class="spinner">
        <div class="double-bounce1"></div>
        <div class="double-bounce2"></div>
    </div>
</div>
<div id="body-wrap">
    @include(template_path_check('/layouts/topmenu'))
    @include(template_path_check('/layouts/breadcrumb'))

    @if ($page)
        @include(template_path_check('/pages/contentview/viewrouter'),['page',$page])
    @endif
    
    @if ($pagecategory)
        @if ($pagecategory->pagecategorydetails)
            
            <div class="speaker_area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12"> 
                            <div class="row">
                                @foreach ($pagecategory->pagecategorydetails as $pagecategorydetail)
                                    <div class="col-md-4 item text-center">
                                            <a href="{{ route('page.category_dfp_download',['id' => $pagecategorydetail->id]) }}">
                                                <div class="b2b-img-countiner">
                                                    <img src="{{ asset('http://travelshopturkey.com'.$pagecategorydetail->image) }}" alt="{{ $pagecategorydetail->title }}" class="b2b-img">
                                                </div>
                                            </a>
                                                
                                            <div class="gal-item-desc delay-1">
                                                <div class="row">
                                                    <div class="col-md-12 text-center">
                                                        <a href="{{ route('page.category_dfp_download',['id' => $pagecategorydetail->id]) }}">
                                                            <h4 class="b2b-h4"><b>{{ $pagecategorydetail->title }}</b></h4>
                                                        </a>
                                                    </div>
                                                    <div class="col-md-12 text-center">
                                                        <a class="btn btn-primary b2b-btn" target="_blank" href="{{ route('page.category_dfp_download',['id' => $pagecategorydetail->id]) }}">@lang('site.b2b_download')</a>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        @endif

    @endif
    

</div>
@endsection