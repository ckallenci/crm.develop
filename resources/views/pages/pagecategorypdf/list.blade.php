@if ($pagecategories)

    <div class="speaker_area">
        <div class="container">
            <div class="row">
                <div class="col-md-12"> 
                    <div class="row">
                        @foreach ($pagecategories as $pagecategory)
                            <div class="col-md-4 item text-center">
                                <a href="{{ route('a.pagedetail', ['id' => $page->id, 'name' => str_slug($page->title), 'id2' => $pagecategory->id, 'name2' => str_slug($pagecategory->title)]) }}">
                                    <div class="b2b-img-countiner">
                                        <img src="{{ asset($pagecategory->image) }}" alt="{{ $pagecategory->title }}" class="b2b-img">
                                    </div>
                                </a>
                                
                                    <div class="gal-item-desc delay-1">
                                        <div class="row">
                                            <div class="col-md-8 text-left">
                                                    <a href="{{ route('a.pagedetail', ['id' => $page->id, 'name' => str_slug($page->title), 'id2' => $pagecategory->id, 'name2' => str_slug($pagecategory->title)]) }}">
                                                <h4 class="b2b-h4"><b>{{ $pagecategory->title }}</b></h4>
                                                    </a>
                                            </div>
                                            <div class="col-md-4 text-right">
                                                <a class="btn btn-primary b2b-btn" href="{{ route('a.pagedetail', ['id' => $page->id, 'name' => str_slug($page->title), 'id2' => $pagecategory->id, 'name2' => str_slug($pagecategory->title)]) }}">@lang('site.b2b_see_tours')</a>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div> 
@endif

