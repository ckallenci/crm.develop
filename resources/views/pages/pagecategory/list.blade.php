<style>
    
</style>

@if ($countriesdata)

    <div class="speaker_area">
        <div class="container">
            <div class="row">
                <div class="col-md-12"> 
                    <div class="row">
                        @foreach ($countriesdata as $country_item)
                            <div class="col-md-4 item text-center">
                                <a href="{{ route('a.page', ['id' => $pageitem->id, 'name' => str_slug($pageitem->title), 'tour_name_like' => '' , 'continent_id' => $req['continent_id'], 'country_id' => $country_item['data']->tour_country_id ]) }}">
                                    <img src="{{ image_check('https://travelshoptours.com/assets/public/files/thumbs/'.$country_item['data']->image) }}" alt="{{ $country_item['data']->name }}" class="img-fluid img-full">
                                    <div class="tour-layer delay-1"></div>
                                    <div class="gal-item-desc delay-1">
                                        <h4><b>{{ $country_item['data']->name}}</b></h4>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@elseif ($continents)
    <div class="speaker_area">
        <div class="container">
            <div class="row">
                <div class="col-md-12"> 
                    <div class="row">
                        @foreach ($continents as $category_item)
                            @if ($category_item->image)
                                <div class="col-md-4 item text-center">
                                    <a href="{{ route('a.page', ['id' => $page->id, 'name' => str_slug($page->title), 'continent_id' => $category_item->id]) }}">
                                        <img src="{{ image_check('https://travelshoptours.com/assets/public/files/thumbs/'. $category_item->image) }}" alt="{{ $category_item->name }}" class="img-fluid img-full">
                                        <div class="tour-layer delay-1"></div>
                                        <div class="gal-item-desc delay-1">
                                            <h4><b>{{ $category_item->name}}</b></h4>					
                                            <h5 class=""> {{ substr($category_item->description, 0, 37)."..." }} </h5>
                                        </div>
                                    </a>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div> 
@endif

