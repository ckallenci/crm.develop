<div id="slider"></div>
<div class="sliderbar">
        <div class="container-fluid">
              <div class="row">
                  <div class="col-md-12 text-right">
                      <a href="javascript:void(0)" class="sliderbtn">@lang('site.sliderbtn')</a>
                  </div>
              </div>
          </div>
      </div>
<div id="appointment">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 d-flex align-items-center">
                <div class="row">
                    <div class="offset-xl-2 col-xl-9 offset-lg-3 col-lg-6 offset-md-3 col-md-6">
                        {!! Form::open(['route' => "appointment.store", 'method' => "POST", "class" =>"appointmnent", "id" =>"appointmnent"]) !!}
                            <div class="row">
                                <div class="col-md-12">
                                    <h1 class="slidertitle">@lang('site.sliderbtn')</h1>
                                </div>
                                <div class="col-md-8">
                                    {{ Form::dDateTime2('date', null, trans('site.appointmnent_date')) }}
                                </div>
                                <div class="col-md-4">
                                    {{ Form::dSelect2('time', null, array() ,trans('site.appointmnent_time')) }}
                                </div>
                                <div class="col-md-12">
                                    {{ Form::dText2('name',null , trans('site.appointmnent_name')) }}
                                </div>
                                <div class="col-md-12">
                                    {{ Form::dText2('phone',null , trans('site.appointmnent_phone')) }}
                                </div>
                                <div class="col-md-12">
                                    {{ Form::dTextarea2('note',null , trans('site.appointmnent_note')) }}
                                </div>
                                <div class="col-md-12 text-right">
                                    {{ Form::dSubmit2('note',trans('site.appointmnent_btn')) }}
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <div class="col-md-6 form-bg"></div>
        </div>
    </div>
</div>
@push('scripts')
    <script>
        var ranges = [ {start: new Date({{date('Y',strtotime($firstday))}},{{date('n',strtotime($firstday))}}-1,{{date('d',strtotime($firstday))}}), end: new Date({{date('Y',strtotime($endday))}},{{date('n',strtotime($endday))}}-1,{{date('d',strtotime($endday))}})},];
        var activeday = {!! $datelist !!};
        function datecheck(date) {
            for(var i=0; i < activeday.length; ++i) {
               if (activeday[i] == date) {
                    return true;
                }
            }
            return false;
        }
        $('.sliderbtn').on('click',function() {
            $('#slider').hide();
            $('.sliderbtn').hide();
            $('#appointment').show();
        });
        $('.date').datepicker({
            format: 'dd/mm/yyyy',
            startDate: ranges[0].start,
            endDate: ranges[ranges.length -1].end,
            datesDisabled: {!! $disableList !!},
        }).on('changeDate', function (ev) {
            $.ajax({
                method: "POST",
                url: '{{ route("hour") }}',
                dataType:'json',
                data: { date: ev.target.value },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            .done(function( data ) {
                var time = $("#time");
                time.find('option').remove().end().append('<option value="">{{ trans("site.appointmnent_time") }}</option>').val('');
                for (k = 0; k < data.length; k++){
                    time.append(
                        $('<option></option>').val(data[k]).html(data[k])
                    );
                }
            });
        });
        $('#appointmnent').on('submit',function(event){
            $.ajax({
                method: $(this).attr('method'),
                url: $(this).attr('action'),
                dataType:'json',
                data: $(this).serialize(),
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {
                    window.location.href = data;
                },
                error: function(errors) {
                    $('.invalid-feedback').remove();
                    $.each(errors.responseJSON.errors, function(key,val) {
                        $('#'+key).after( '<div class="invalid-feedback">'+val+'</div>' );
                        $('#'+key+' .invalid-feedback').show();
                    });
                    $('.invalid-feedback').show();
                },
            })
            /*.success(function() {
                alert( "success" );
            })
            .fail(function(errors) {
                $('.invalid-feedback').remove();
                $.each(errors.responseJSON.errors, function(key,val) {
                    $('#'+key).after( '<div class="invalid-feedback">'+val+'</div>' );
                    $('#'+key+' .invalid-feedback').show();
                });
                $('.invalid-feedback').show();
            })*/
            return false;
        });
        $(document).ready(function(){
            $('#phone').inputmask("0(999) 999 99 99");  //static mask
        });
    </script>
@endpush
	
@push('scripts')

    <script>
lottie.loadAnimation({
    container: document.getElementById('slider'), // the dom element that will contain the animation
    renderer: 'svg',
    loop: false,
    autoplay: true,
    path: "{{ asset('data.json') }}" // the path to the animation json
  });
  $( document ).ready(function() {
    var h = $(window).height();
    $('#appointment').css('min-height',h);
    $('.form-bg').css('min-height',h);
  });
</script>
@endpush
