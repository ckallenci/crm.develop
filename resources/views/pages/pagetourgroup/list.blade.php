@if ($tour_detail)
<div class="venues_area">
    <div class="overlay"></div>
     <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="heading-title text-center" >
                    <h2 class="f-weight-700 margin-0"> {{ $tourgroupsitem->name }} </h2>
                    <div class="bordershep"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="venues_slider owl-carousel">
                @foreach ($tour_detail as $touritem)
                    <div class="item">
                        <div class="venues-item">
                            <div class="single-venues-item">
                                <div class="thumb">
                                    <a href="{{ route('a.pagedetail',['name'=>str_slug($pageitem->title),'id' => $pageitem->id,'name2' =>str_slug($touritem['Name']),'id2' => $touritem['ID'] ])}}">
                                        <img src="{{ image_check($touritem['Image_Long']) }}" alt="{{ $touritem['Name'] }}" class="img-fluid" />
                                    </a>
                                    <div class="box b1">
                                        <span> {!! PriceCurrency($touritem['Invoice_Currency'], $touritem['Price_Default']) !!}</span>
                                    </div>
                                </div>
                                <div class="content content_size">
                                    <a href="{{ route('a.pagedetail',['name'=>str_slug($pageitem->title),'id' => $pageitem->id,'name2' =>str_slug($touritem['Name']),'id2' => $touritem['ID'] ])}}">
                                        <h3 class="title f-weight-700"> {{ $touritem['Name'] }} </h3>
                                    </a>
                                    <p>
                                        {{ trans('site.touritem_from_from') }} : <a href="{{ route('a.page',['name'=>str_slug($pageitem->title),'id' => $pageitem->id,'tour_name_like' =>$touritem['Tour_Start_City'] ])}}" class="from_to_color">{{ $touritem['Tour_Start_City'] }}</a> <br>
                                        {{ trans('site.touritem_from_to') }} : <a href="{{ route('a.page',['name'=>str_slug($pageitem->title),'id' => $pageitem->id,'tour_name_like' =>$touritem['Tour_End_City'] ])}}" class="from_to_color">{{ $touritem['Tour_End_City'] }}</a> 
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </div>
</div>
   
@endif
<!--End venues Area-->
@section('js')
    <script>
    $( document ).ready(function() {
        $('#continent_id').on('change',function() {
            var continent_id = $('#continent_id').val();
            $.ajax({
                type: "GET",
                url: "{{ route('ajax.tourcountry') }}",
                data:'continent_id='+continent_id,
                success: function(data){
                    $('#country_id').empty();
                    $('#country_id').append($('<option>').text('').attr('value', ""));
                    $.each(data, function(i, value) {
                       $('#country_id').append($('<option>').text(value).attr('value', i));
                });
                }
            });
        });
    });
            </script>
@endsection