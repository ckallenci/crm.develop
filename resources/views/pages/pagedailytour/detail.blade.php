@extends($extend_uzanti)

@section('content')
<div class="site-preloader">
    <div class="spinner">
        <div class="double-bounce1"></div>
        <div class="double-bounce2"></div>
    </div>
</div>
<div id="body-wrap">
    @include(template_path_check('/layouts/topmenu'))
    @include(template_path_check('/layouts/breadcrumb'))
    
@if ($tour)
    <div class="speaker_area tour-detail">
		<div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="tour-detail-title">{{ $tour->tourInfo->Name }}</h1>
                </div>
            </div>
            <?php
                if($tour->tourInfo->Is_Daily_Tour == 1){
                    $calculation_href = route('ajax.tour_calculation_daily_tour');
                    
                }else{
                    $calculation_href = route('ajax.tour_calculation');
                }
                ?>
            <div class="row"> 
                    <div class="col-md-4 order-md-12">
                            <div class="booking">BOOKING</div>
                            <div class="cal">
                            <form id="bookingform" method="post" action="{{ route('booking.page') }}" autocomplete="off">
                                    {{ csrf_field() }}
                                    <?php $linkimiz='http://'.$_SERVER['HTTP_HOST'].''.$_SERVER['REQUEST_URI']; ?>
                                    <input type="hidden" name="tour_id" id="tour_id" value="{{ $tour->tourInfo->ID }}">
                                    <input type="hidden" name="tour_names" value="{{ $tour->tourInfo->Name }}">
                                    <input type="hidden" name="linkimiz"  value="{{ $linkimiz }}" class="form-control" />
                                    <input type="hidden" name="site"  value="{{ route('index') }}" class="form-control" />
                                    <input type="hidden" name="tour_img"  value="{{ $tour->_config_data_->image_path . $tour->tourInfo->Image_Long }}" class="form-control" />
                            @if ($tour->tourInfo->Discount_Flat != "0.00")
                                
                                <h4 class="tourdiscount">YOU HAVE <span class="red">{{ $tour->tourInfo->Discount_Flat}}%</span> DISCOUNT.</h4>
                            @endif    
                            @if (Auth::user())
                                @if (Auth::user()->user_informations)
                                    <h4 class="tourdiscount">YOU HAVE <span class="red">{{ Auth::user()->user_informations->discount_rate }}%</span> COMMISION.</h4>
                                @endif
                            @endif            
                           
                            <div class="booking-price text-center">
                                Starts from
                                                
                                <span class="black">
                                <?php
                                $itsPriceforit = $tour->tourInfo->Price_Default;
                                
                                $Fiyat 			= $itsPriceforit;
                                $Oran    		= $tour->tourInfo->Discount_Flat;
                                $InenTutar 		= $Fiyat/100*$Oran;
                                $KalanTutar 	= $Fiyat-$InenTutar;
                                ?>
                                {!! PriceCurrency($tour->tourInfo->Invoice_Currency,$KalanTutar) !!}    
                                </span>	
                            </div>
                                              
                               
                            
                            <input type="hidden" name="promotional_discount"  value="{{$tour->tourInfo->Discount_Flat}}" class="form-control" />
                            @if (Auth::user())
                                @if (Auth::user()->user_informations)
                                    <input type="hidden" name="agency_discount"  value="{{ Auth::user()->user_informations->discount_rate }}" class="form-control" />
                                @else
                                    <input type="hidden" name="agency_discount"  value="{{ 0 }}" class="form-control" />
                                @endif
                                
                            @else
                                <input type="hidden" name="agency_discount"  value="0" class="form-control" />
                            @endif
                        
                            <input type="hidden" name="total_discount"  value="{{ $tour->tourInfo->Discount_Flat}}" class="form-control" />
                            
                            <div class="hpadding5">
                
                            <hr>
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="form-group">
                                        <label><i class="far fa-calendar-alt"></i> Departing date</label>
                                    <input type="text" style="cursor: pointer;" class="form-control required mySelectCalendar " value="" name="date" id="date" readonly onChange="xmlhttpPost('{{ $calculation_href }}'); return false;" />                       
                                       
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12">
                                    <div class="form-group">
                                
                                        <label><i class=" icon-clock"></i> Tour Type</label>
                                    <?php
                                $sayptype=count($tour->tourPrices);
                                $sayptype=$sayptype-1;
                                for ($i = 0; $i <= $sayptype; $i++) {
                                $tptype[]= $tour->tourPrices[$i]->Offer_Type_Name;
                                }
                                
                                
                                ?>					
                                    <select class="form-control required" name="tourtype" id="tourtype" onChange="xmlhttpPost('{{ $calculation_href }}'); return false;">
                                        <option value="0" >Select</option>
                                        <?php
                                if (in_array("Private", $tptype) and in_array("Regular", $tptype)) {
                                    echo '<option value="1">Regular Tour</option><option value="2">Private Tour</option>';
                                }
                                elseif (in_array("Private", $tptype)) {
                                echo '<option value="2">Private Tour</option>';
                                }
                                elseif (in_array("Regular", $tptype)) {
                                echo '<option value="1">Regular Tour</option>';
                                } 
                                else "";
                                ?>
                                    
                                    </select>
                      
                             </div>
                                </div>
                            </div>
                           
                            <div class="row">
                                @if ($tour->tourInfo->Is_Daily_Tour != 1)
                                    
                                <div class="col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <label>Hotel Star</label>
                                        <select class="form-control required" id="accom" name="accom" onChange="xmlhttpPost('{{ $calculation_href }}'); return false;">
                                        <?
                                        $UcStar 	= "Bos";
                                        $DortStar 	= "Bos";
                                        $BesStar 	= "Bos";
                                        
                                        foreach($tour->tourPrices as $detay){
                                            if(($detay->Amount_3_Star_Single != "0.00" || $detay->Amount_3_Star_Shared_Double != "0.00" || $detay->Amount_3_Star_Triple != "0.00") && $UcStar == "Bos"){
                                                echo "<option value='2'>3 Star Hotel</option>"; 
                                                $UcStar = "Dolu";
                                                }
                                            if(($detay->Amount_4_Star_Single != "0.00" || $detay->Amount_4_Star_Shared_Double != "0.00" || $detay->Amount_4_Star_Triple != "0.00") && $DortStar == "Bos"){
                                                echo "<option value='3'>4 Star Hotel</option>";
                                                $DortStar = "Dolu";
                                                }
                                            elseif(($detay->Amount_5_Star_Single != "0.00" || $detay->Amount_5_Star_Shared_Double != "0.00" || $detay->Amount_5_Star_Triple != "0.00") && $BesStar == "Bos"){
                                                echo "<option value='4'>5 Star Hotel</option>"; 
                                                $BesStar = "Dolu";
                                                }
                                        }
                                        ?>
                                        </select>
                                    </div>
                                </div>
                              
                                @endif
                                
                        
        
                                        
                                        @if ($tour->tourInfo->Is_Daily_Tour!=1)
                                            
                                            <div class="col-md-4 col-sm-4">
                                                <div class="form-group">
                                                <label>Adults</label>
                                                <select class="form-control required" id="pax" name="pax" >
                                                    <option value="0">Select</option>
                                                    @for ($i = 1; $i < 41; $i++)
                                                        <option value="{{ $i }}">{{ $i }}</option>
                                                    @endfor
                                                </select>
                                                </div>
                                            </div>
                                        @else
                                            <div class="col-md-6 col-sm-6">
                                                <div class="form-group">
                                                <label>Adults</label>
                                                <select class="form-control required" id="pax" name="pax" onChange="xmlhttpPost('{{ $calculation_href }}'); return false;">
                                                    <option value="0">Select</option>
                                                    @for ($i = 1; $i < 41; $i++)
                                                        <option value="{{ $i }}">{{ $i }}</option>
                                                    @endfor
                                                </select>
                                                </div>
                                            </div>
                                        @endif
                
                                
                                        @if ($tour->tourInfo->Is_Daily_Tour!=1)
                                            <div class="col-md-4 col-sm-4">
                                                <div class="form-group">
                                                    <label>Children</label>
                                                    <select class="form-control required" id="children" name="children">
                                                        <option value="0">Select</option>
                                                        @for ($i = 1; $i < 16; $i++)
                                                            <option value="{{ $i }}">{{ $i }}</option>
                                                        @endfor
                                                    
                                                    </select>
                                                </div>
                                                </div>
                                            </div>
                                        @else

                                            <div class="col-md-6 col-sm-6">
                                                <div class="form-group">
                                                    <label>Children</label>
                                                    <select class="form-control required" id="children" name="children">
                                                        <option value="0">Select</option>
                                                        @for ($i = 1; $i < 16; $i++)
                                                        <option value="{{ $i }}">{{ $i }}</option>
                                                    @endfor
                                                    </select>
                                                    <input type="hidden" name="is_daily_tour" value="yes" class="form-control" />
                                                    </div>
                                                </div>
                                            </div>
                                        
                                        @endif
                                        
                                    
                
                            
                             <div id="child"></div>
                             <div id="paxa"></div>
                            <br>
                                
                            <div id="CalcResult">
                            <div class="col-sm-12 col-xs-12" id="noPriceDiv" style="display: block;"><b><br />Please select tour <u>Date</u>, <u>Type</u> and <u> Adults</u></b><br /><br /></div>
                            
                            </div>
                            
                   
                          </form>
                                
                                
                                </div>
                            @endif
                                
                            </div>
                        </div>
               
                    
                    
                    
            
                    
                
                
                    
                 <div class="col-md-8 order-md-1">
                    <div class="connected-carousels">
                        <div class="stage">
                            <div class="carousel carousel-stage">
                                <ul>
                                @if (isset($tour->tourGalleryItems))
                                @foreach ($tour->tourGalleryItems as $item)
                                    <li><img src="{{ $tour->_config_data_->image_path.$item->Image }}" alt="{{ $item->Name }}"></li>
                                @endforeach
                                @endif
                                
                                </ul>
                            </div>
                            <a href="#" class="prev prev-stage"><span>&lsaquo;</span></a>
                            <a href="#" class="next next-stage"><span>&rsaquo;</span></a>
                        </div>
                        <div class="navigation">
                            <a href="#" class="prev prev-navigation">&lsaquo;</a>
                            <a href="#" class="next next-navigation">&rsaquo;</a>
                            <div class="carousel carousel-navigation">
                                <ul>
                                    @if (isset($tour->tourGalleryItems))
                                    @foreach ($tour->tourGalleryItems as $item)
                                        <li><img src="{{ $tour->_config_data_->thumb_image_path.$item->Image }}" alt="{{ $item->Name }}" ></li>
                                    @endforeach
                                    @endif                                
                                </ul>
                            </div>
                        </div>
                    </div>
                    <ul class="nav nav-tabs tabs-style-topline" id="myTab" role="tablist">
                            <li class="nav-item">
                              <a class="nav-link active" id="itinerary-tab" data-toggle="tab" href="#itinerary" role="tab" aria-controls="itinerary" aria-selected="true"><i class="fas fa-info"></i> Itinerary</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" id="facilities-tab" data-toggle="tab" href="#facilities" role="tab" aria-controls="facilities" aria-selected="false"><i class="fas fa-bookmark"></i> Facilities</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" id="terms-tab" data-toggle="tab" href="#terms" role="tab" aria-controls="terms" aria-selected="false"><i class="fas fa-file-alt"></i> Terms & Conditions</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" id="faq-tab" data-toggle="tab" href="#faq" role="tab" aria-controls="faq" aria-selected="false"><i class="fas fa-question-circle"></i> FAQ</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" id="available-tab" data-toggle="tab" href="#available" role="tab" aria-controls="available" aria-selected="false"><i class="fas fa-leaf"></i> Available</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" id="hotels-tab" data-toggle="tab" href="#hotels" role="tab" aria-controls="hotels" aria-selected="false"><i class="fas fa-hotel"></i> Hotels</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="maps-tab" data-toggle="tab" href="#maps" role="tab" aria-controls="maps" aria-selected="false"><i class="fas fa-map"></i> Maps</a>
                            </li>
                          </ul>
                          <div class="tab-content tabs-style-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="itinerary" role="tabpanel" aria-labelledby="itinerary-tab">
                                <div class="accordion" id="accordionExample">
                                  
                                    <div class="card">
                                        <div class="card-header" id="headinghiglight">
                                            <h2 class="mb-0">
                                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapsehiglight" aria-expanded="true" aria-controls="collapsehiglight">
                                                        Highlights
                                                        <span class="fas fa-angle-down"></span>
                                                </button>
                                                
                                            </h2>
                                        </div>
                                        <div id="collapsehiglight" class="collapse show " aria-labelledby="headinghiglight" data-parent="#accordionExample">
                                            <div class="card-body">
                                                    {{ $tour->tourInfo->Highlights }}
                                            </div>
                                        </div>
                                    </div>
                                @foreach ($tour->tourItinerary as $key => $itinerary)
                                   
                                    <div class="card">
                                        <div class="card-header" id="heading{{ $key }}">
                                            <h2 class="mb-0">
                                                <button class="btn btn-link  collapsed" type="button" data-toggle="collapse" data-target="#collapse{{ $key }}" aria-expanded="true" aria-controls="collapse{{ $key }}">
                                                        {{$itinerary->Name}}
                                                        <span class="fas fa-angle-down"></span>
                                                </button>
                                                
                                            </h2>
                                        </div>
                                        <div id="collapse{{ $key }}" class="collapse " aria-labelledby="heading{{ $key }}" data-parent="#accordionExample">
                                            <div class="card-body">
                                                    @if (meals($itinerary->Include_Welcome_Drink,$itinerary->Include_Breakfast,$itinerary->Include_Lunch,$itinerary->Include_Dinner)!="")
                                                        <span class="meals">{{meals($itinerary->Include_Welcome_Drink,$itinerary->Include_Breakfast,$itinerary->Include_Lunch,$itinerary->Include_Dinner)}}</span>                                                        
                                                    @endif
                                                    {{ $itinerary->Description }}
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                    
                                </div>
                            </div>
                            <div class="tab-pane fade" id="facilities" role="tabpanel" aria-labelledby="facilities-tab">
                               
                                <h4 class="tour-detail-subtitle dark">Tour Includes</h4> 
                                <?php  
                                $Facilities_Include_Old=preg_replace('/[^\x20-\x7E]/','-', $tour->tourInfo->Facilities_Include_Old); 
                                $Facilities_Include_Old=str_replace("--------","<br />&bull; ",$Facilities_Include_Old);
                                $Facilities_Include_Old=str_replace("--"," ",$Facilities_Include_Old);
                                $Facilities_Include_Old=str_replace("&bull;","<br />",$Facilities_Include_Old);
                                ?>
                                {!!$Facilities_Include_Old!!}
                                <br/>
                                <br/>
                                    <h4 class="tour-detail-subtitle dark">Tour Does Not Include</h4> 
                                    <?php
                                $Facilities_Exclude_Old=preg_replace('/[^\x20-\x7E]/','-', $tour->tourInfo->Facilities_Exclude_Old); 
                                $Facilities_Exclude_Old=str_replace("--------","<br />&bull; ",$Facilities_Exclude_Old);
                                $Facilities_Exclude_Old=str_replace("--"," ",$Facilities_Exclude_Old);
                                $Facilities_Exclude_Old=str_replace("&bull;","<br />",$Facilities_Exclude_Old);
                                ?>
                                {!!$Facilities_Exclude_Old!!}
                            </div>
                            <div class="tab-pane fade" id="terms" role="tabpanel" aria-labelledby="terms-tab">
                                {{$tour->tourInfo->Terms_Conditions}}
                            </div>
                            <div class="tab-pane fade" id="faq" role="tabpanel" aria-labelledby="faq-tab">
                                {{$tour->tourInfo->Faq}}
                            </div>
                            <div class="tab-pane fade" id="available" role="tabpanel" aria-labelledby="available-tab">
                                    
                                    <h3 class="tour-detail-subtitle">Available On Weekdays</h3>
                                    <?php
                                    if(isset($tour->tourInfo->Available_On_Weekdays)){
                                        $days = $tour->tourInfo->Available_On_Weekdays;
                                        $gunler = explode(",", $days);
                                        
                                        foreach($gunler as $gun){
                                            //print_r($gun);
                                            $gun = str_replace('"','',$gun);
                                            //$gun = explode(',',$gun);
                                            if($gun == 1){echo "<b>Sunday</b> <br />";}
                                            if($gun == 2){echo "<b>Monday</b> <br />";}
                                            if($gun == 3){echo "<b>Tuesday</b> <br />";}
                                            if($gun == 4){echo "<b>Wednesday</b> <br />";}
                                            if($gun == 5){echo "<b>Thursday</b> <br />";}
                                            if($gun == 6){echo "<b>Friday</b> <br />";}
                                            if($gun == 7){echo "<b>Saturday</b> <br />";}
                                            
                                        }
                                    }
                                    ?>
                                    <br/>
                                    <h3 class="tour-detail-subtitle">Available On Dates</h3>
                                    
                                    <?php
                                    if(isset($tour->tourInfo->Available_On_Dates) && count($tour->tourInfo->Available_On_Dates)){
                                        
                                            foreach($tour->tourInfo->Available_On_Dates as $aod){
                                                
                                                $Date_From    = date("j F, Y", strtotime($aod->Date_From));
                                                $Date_To     = date("j F, Y", strtotime($aod->Date_To));
                                                
                                                echo $Date_From . "<br />";
                                            } 
                                        } 
                                    ?>
                            </div>
                            <div class="tab-pane fade" id="hotels" role="tabpanel" aria-labelledby="hotels-tab">
                                {!! $tour->tourInfo->Hotels_Manual !!}
                            </div>
                            <div class="tab-pane fade" id="maps" role="tabpanel" aria-labelledby="maps-tab">
                                    <div id="map"></div>
                                  
                                    
                            </div>
                        </div>
                </div>
                </div>
										
                
            </div>
        </div>
    </div>
</div>

</div>
@endsection
@section('js')
    <script>
        (function($) {
    // This is the connector function.
    // It connects one item from the navigation carousel to one item from the
    // stage carousel.
    // The default behaviour is, to connect items with the same index from both
    // carousels. This might _not_ work with circular carousels!
    var connector = function(itemNavigation, carouselStage) {
        return carouselStage.jcarousel('items').eq(itemNavigation.index());
    };

    $(function() {

        var jcarousel = $('.carousel-stage');

        jcarousel
            .on('jcarousel:reload jcarousel:create', function () {
                var carousel = $(this),
                    width = carousel.innerWidth();


                carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');
            })
            .jcarousel({
                animation: {
                    duration: 500,
                    easing:   'linear',
                    complete: function() {
                    }
                },
                wrap: 'circular'
            })
            .jcarouselAutoscroll({
                interval: 3000,
                target: '+=1',
                autostart: true
            });
        // Setup the carousels. Adjust the options for both carousels here.
        var carouselStage      = $('.carousel-stage').jcarousel();
        var carouselNavigation = $('.carousel-navigation').jcarousel();

        // We loop through the items of the navigation carousel and set it up
        // as a control for an item from the stage carousel.
        
        
        carouselNavigation.jcarousel('items').each(function() {
            var item = $(this);
            
            // This is where we actually connect to items.
            var target = connector(item, carouselStage);
            
            item
                .on('jcarouselcontrol:active', function() {
                    carouselNavigation.jcarousel('scrollIntoView', this);
                    item.addClass('active');
                    
                })
                .on('jcarouselcontrol:inactive', function() {
                    item.removeClass('active');
                })
                .jcarouselControl({
                    target: target,
                    carousel: carouselStage
                });
        });

        // Setup controls for the stage carousel
        $('.prev-stage')
            .on('jcarouselcontrol:inactive', function() {
                $(this).addClass('inactive');
            })
            .on('jcarouselcontrol:active', function() {
                $(this).removeClass('inactive');
            })
            .jcarouselControl({
                target: '-=1'
            });

        $('.next-stage')
            .on('jcarouselcontrol:inactive', function() {
                $(this).addClass('inactive');
            })
            .on('jcarouselcontrol:active', function() {
                $(this).removeClass('inactive');
            })
            .jcarouselControl({
                target: '+=1'
            });

        // Setup controls for the navigation carousel
        $('.prev-navigation')
            .on('jcarouselcontrol:inactive', function() {
                $(this).addClass('inactive');
            })
            .on('jcarouselcontrol:active', function() {
                $(this).removeClass('inactive');
            })
            .jcarouselControl({
                target: '-=1'
            });

        $('.next-navigation')
            .on('jcarouselcontrol:inactive', function() {
                $(this).addClass('inactive');
            })
            .on('jcarouselcontrol:active', function() {
                $(this).removeClass('inactive');
            })
            .jcarouselControl({
                target: '+=1'
            });
    });
})(jQuery);
    </script>
   
     <script>
        // This example adds an animated symbol to a polyline.
        var coordinates = {!! str_replace('"','',$tour->tourInfo->Coordinates) !!};
        var vlat = 0, vlng = 0;
        var minlat=minlng =999990;
        var maxlat=maxlng =-999990;
        for (i = 0; i < coordinates.length; i++) {
            vlat += coordinates[i].lat;
            vlng += coordinates[i].lng;
            if(minlat>coordinates[i].lat){minlat=coordinates[i].lat} if(minlng>coordinates[i].lng){minlng=coordinates[i].lng}
            if(maxlat<coordinates[i].lat){maxlat=coordinates[i].lat} if(maxlng<coordinates[i].lng){maxlng=coordinates[i].lng}
        }

        vlat = vlat / coordinates.length;
        vlng = vlng / coordinates.length;
        
        var zoomlat = (maxlat-minlat);
        var zoomlng = (maxlng-minlng);
        zoomx = 20;
        zoomy = 20;
        if(0.001016664<zoomlng && zoomlng<0.002033328){zoomx=19}
        if(0.002033328<zoomlng && zoomlng<0.004066657){zoomx=18}
        if(0.004066657<zoomlng && zoomlng<0.008133313){zoomx=17}
        if(0.008133313<zoomlng && zoomlng<0.016266627){zoomx=16}
        if(0.016266627<zoomlng && zoomlng<0.032533253){zoomx=15}
        if(0.032533253<zoomlng && zoomlng<0.065066506){zoomx=14}
        if(0.065066506<zoomlng && zoomlng<0.130133013){zoomx=13}
        if(0.130133013<zoomlng && zoomlng<0.260266026){zoomx=12}
        if(0.260266026<zoomlng && zoomlng<0.520532051){zoomx=11}
        if(0.520532051<zoomlng && zoomlng<1.041064102){zoomx=10}
        if(1.041064102<zoomlng && zoomlng<2.082128205){zoomx=9}
        if(2.082128205<zoomlng && zoomlng<4.164256409){zoomx=8}
        if(4.164256409<zoomlng && zoomlng<8.328512817){zoomx=7}
        if(8.328512817<zoomlng && zoomlng<16.65702563){zoomx=6}
        if(16.65702563<zoomlng && zoomlng<33.31405127){zoomx=5}
        if(33.31405127<zoomlng && zoomlng<66.62810254){zoomx=4}
        if(66.62810254<zoomlng && zoomlng<133.256205){zoomx=3}
        if(133.256205<zoomlng && zoomlng<266.5124102){zoomx=2}
        if(266.5124102<zoomlng && zoomlng<533.0248203){zoomx=1}

        if(0.001016664<zoomlat && zoomlat<0.002033328){zoomy=17}
        if(0.002033328<zoomlat && zoomlat<0.004066657){zoomy=16}
        if(0.004066657<zoomlat && zoomlat<0.008133313){zoomy=15}
        if(0.008133313<zoomlat && zoomlat<0.016266627){zoomy=14}
        if(0.016266627<zoomlat && zoomlat<0.032533253){zoomy=13}
        if(0.032533253<zoomlat && zoomlat<0.065066506){zoomy=12}
        if(0.065066506<zoomlat && zoomlat<0.130133013){zoomy=11}
        if(0.130133013<zoomlat && zoomlat<0.260266026){zoomy=10}
        if(0.260266026<zoomlat && zoomlat<0.520532051){zoomy=9}
        if(0.520532051<zoomlat && zoomlat<1.041064102){zoomy=8}
        if(1.041064102<zoomlat && zoomlat<2.082128205){zoomy=7}
        if(2.082128205<zoomlat && zoomlat<4.164256409){zoomy=6}
        if(4.164256409<zoomlat && zoomlat<8.328512817){zoomy=5}
        if(8.328512817<zoomlat && zoomlat<16.65702563){zoomy=4}
        if(16.65702563<zoomlat && zoomlat<33.31405127){zoomy=3}
        if(33.31405127<zoomlat && zoomlat<66.62810254){zoomy=2}
        if(66.62810254<zoomlat && zoomlat<133.256205){zoomy=1}
        if(133.256205<zoomlat && zoomlat<266.5124102){zoomy=1}
        if(266.5124102<zoomlat && zoomlat<533.0248203){zoomy=1}
        var zoom = 0;
        if (zoomx<zoomy) {
            zoom = zoomx;
        }else{
            zoom = zoomy;
        }
        function initMap() {
            var map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: (maxlat+minlat)/2, lng: (maxlng+minlng)/2},
                zoom: zoom,
                mapTypeId: 'terrain'
            });
            // Define the symbol, using one of the predefined paths ('CIRCLE')
            // supplied by the Google Maps JavaScript API.
            var lineSymbol = {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 8,
                strokeColor: '#393'
            };
            // Create the polyline and add the symbol to it via the 'icons' property.
            var line = new google.maps.Polyline({
                    path: {!! str_replace('"','',$tour->tourInfo->Coordinates) !!},
                    icons: [{
                    icon: lineSymbol,
                    offset: '100%'
                }],
                map: map
            });
            animateCircle(line);
        }
        function animateCircle(line) {
            var count = 0;
            window.setInterval(function() {
                count = (count + 1) % 200;
                var icons = line.get('icons');
                icons[0].offset = (count / 2) + '%';
                line.set('icons', icons);
            }, 50);
        }
        </script>
        <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCO4eOH1jJxLbcmp19X7AVF1jGryzeESEM&callback=initMap">
        </script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
   
    <?php
    foreach($tour->tourInfo->Available_On_Dates as $avdates){ 
        if ($avdates->Date_To!=null) {
            
        
        $avdatesfrom=$avdates->Date_From;
       
        $bugun = date('Y-m-d');
        if($avdatesfrom <= $bugun){
            $avdatesfrom=$bugun;
            
        }else{
            $avdatesfrom=$avdates->Date_From;
        }
        
        $avdatesto=$avdates->Date_To;
        
        $virgfrom = implode(" ", xml2array($avdatesfrom));
        
        $df = explode("-", $virgfrom);
        
        $ayfrom = $df[1]-1;
        $gunfrom = $df[2];
        $yilfrom= $df[0];
        $tarihyenifrom[]=array($yilfrom.",".$ayfrom.",".$gunfrom);
       
        
        $virgto = implode(" ", xml2array($avdates->Date_To));
        $dt = explode("-", $virgto);
        $ayto = $dt[1]-1;
        $gunto = $dt[2];
        $yilto= $dt[0];
        $tarihyenito[]=array($yilto.",".$ayto.",".$gunto);
    }
    
    }
    
        $tarihyenifrom=array_sort($tarihyenifrom, '', SORT_ASC); 
        $tarihyenito=array_sort($tarihyenito, '', SORT_ASC); 
        $tarihyenifrom = array_values($tarihyenifrom);
        $tarihyenito = array_values($tarihyenito);
    
    $saydates=count($tarihyenito);
    
    
   
    if(isset($tour->tourInfo->Available_On_Weekdays)){
    $days = $tour->tourInfo->Available_On_Weekdays;
    $days = str_replace("'",'',$days);
    
    $gunler = explode(",", $days);
    $gunsayilari = array();
    foreach($gunler as $gun){
        $gun = str_replace('"','',$gun);
        if($gun == 1){$gunname =  "Sunday"; $guncode = 0;}
        if($gun == 2){$gunname =  "Monday"; $guncode = 1;}
        if($gun == 3){$gunname =  "Tuesday"; $guncode = 2;}
        if($gun == 4){$gunname =  "Wednesday"; $guncode = 3;}
        if($gun == 5){$gunname =  "Thursday"; $guncode = 4;}
        if($gun == 6){$gunname =  "Friday"; $guncode = 5;}
        if($gun == 7){$gunname =  "Saturday"; $guncode = 6;}
        array_push($gunsayilari,$guncode);
         
    }
    }
    $bass = json_encode($gunsayilari);
           
    for ($i = 0; $i <= $saydates-1; $i++) {
        
        $dateranges[]="{start: new Date(".$tarihyenifrom[$i][0]."), end: new Date(".$tarihyenito[$i][0].")},";
       
       
    }
    
    $dateranges = implode(" ", $dateranges);
    
    
    
    
    
    ?>
    
    <script>
    // Accepts a Date object or date string that is recognized by the Date.parse() method
    function getDayOfWeek(date) {
      var dayOfWeek = new Date(date).getDay();    
      return isNaN(dayOfWeek) ? null : ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'][dayOfWeek];
    }
    
    
    
      // yıl / ay / gün
      var ranges = [ {{ $dateranges }}];
      
      //köşeli parantezler içerisinde tanımlı
        var days = {{ $bass }};
        $("#date").datepicker({
        dateFormat: 'yy-mm-dd',
        numberOfMonths: 1,
        beforeShowDay: function(date) {
            
            for(var i=0; i<ranges.length; i++) {
                
                
                if(date >= ranges[i].start && date <= ranges[i].end) {
                    
                     var day = date.getDay();
                     
                     if (day == days[0] || day == days[1] || day == days[2] || day == days[3] || day == days[4] || day == days[5] || day == days[6]) {
                     return [true, ''];
                     } else { 
                     return [false, ''];
                     }
            
                }
    
           }
            return [false, ''];
        },
        minDate: ranges[0].start,
        maxDate: ranges[ranges.length -1].end,
        
        
    });
    function xmlhttpPost(strURL) {
        $.ajax({
        method: "POST",
        url: strURL,
        dataType: "json",
        headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
        data: $("#bookingform").serialize(),
        success:function(data) {
            html = '<table class="table table_summary">';
            if(data.pax>0){
                html += '<tr><td>Adult(s)</td> <td class="text-right summary_right">'+data.pax+'<input name="totil" type="hidden" value="'+data.Total_pax+'"></td></tr>';
            }
            if (data.children>0) {
                html += '<tr><td>Children(s)</td><td class="text-right summary_right">'+data.children+'</td></tr>';
            }
            @if ($tour->tourInfo->Is_Daily_Tour != 1)
            if (data.singlea>0) {
                html += '<tr><td>'+data.singlea+' single room(s)</td><td class="text-right summary_right">'+data.singlea_price+'</td></tr>';
            }
            if (data.doublea>0) {
                html += '<tr><td>'+data.doublea+' double room(s)</td><td class="text-right summary_right">'+data.doublea_price+'</td></tr>';
            }
            if (data.triplea>0) {
                html += '<tr><td>'+data.triplea+' triple room(s)</td><td class="text-right summary_right">'+data.triplea_price+'</td></tr>';
            }
            @else
            if (data.Total_pax>0) {
                if (data.pax>1) {
                    html += '<tr><td>'+data.PaxPrice+' x '+data.pax+' </td><td class="text-right summary_right">'+data.Total_pax_price+'</td></tr>';
                } else {
                    html += '<tr><td>1 person price</td><td class="text-right summary_right">'+data.Total_pax_price+'</td></tr>';
                }
            }
            @endif
            if(data.children>0){
                for (let index = 0; index < data.child_price.length; index++) {
                    const element = data.child_price[index];
                    html +='<tr><td><span class="total_price_span text-right">Child '+(parseInt(index)+1)+'</span></td><td class="text-right"><span class="total_price_span text-right summary_right">'+element+'</span></td></tr>';
                }
                
            }
            if (data.hasOwnProperty('sub_total')) {
                html +='<tr><td><span class="total_price_span text-right">Sub Total</span></td><td class="text-right"><input name="totilchild" type="hidden" value="'+data.children+'"><span class="total_price_span text-right summary_right">'+data.sub_total+'</span></td></tr>';
            }		
            if (data.hasOwnProperty('PromoTotal')) {
                html +='<tr><td><span class="total_price_span text-right"><span>'+data.promotional_discount+'%</span> Promotional Discount</span></td><td class="text-right"><input name="totilchild" type="hidden" value="'+data.children+'"><span class="total_price_span text-right summary_right">'+data.PromoTotal+'</span></td></tr>';

                html +='<tr><td><span class="total_price_span text-right">After Promotional Discount</span></td><td class="text-right"><input name="totilchild" type="hidden" value="'+data.children+'"><span class="total_price_span text-right summary_right">'+data.Total_after_Promo+'</span></td></tr>';
            }	
            
            if (data.Total_pax >0) {
                if (data.hasOwnProperty('CommisionTutar')) {
                    if (data.agency_discount>0) {
                        html +='<tr><td><span class="total_price_span text-right">'+data.agency_discount+'%</span> Comission Discount</td><td class="text-right"><span class="total_price_span text-right summary_right">'+data.CommisionTutar+'</span></td></tr>';
                        html +='<tr><td><span class="total_price_span text-right">Net Payable Amount</span></td><td class="text-right"><input name="totilchild" type="hidden" value="'+data.children+'"><span class="total_price_span text-right summary_right">'+data.Total_after_commision+'</span></td></tr>';
                    }
                }
            }
			
			if (data.Total>0) {
                html +='<input type="hidden" name="singlebalance" id="singlebalance" value="'+data.P_Single+'"><input type="hidden" name="doublebalance" id="doublebalance" value="'+data.P_Double+'"><input type="hidden" name="triplebalance" id="triplebalance" value="'+data.P_Triple+'"><input type="hidden" name="child_total" id="child_total" value="'+data.CocukTotal+'"><input type="hidden" name="sroom" id="sroom" value="'+data.singlea+'"><input type="hidden" name="droom" id="droom" value="'+data.doublea+'"><input type="hidden" name="troom" id="troom" value="'+data.triplea+'"><input type="hidden" name="Totalbalance" id="Totalbalance" value="'+data.Total+'"><input type="hidden" name="Totalbalance" id="currency_symbol" value="'+data.currency_code+'"><input type="hidden" name="Price_ID" id="Price_ID" value="'+data.Price_ID+'"><input type="hidden" name="Start_Date" id="Start_Date" value="'+data.Start_Date+'"><input type="hidden" name="End_Date" id="End_Date" value="'+data.End_Date+'">';
            }
            html +='</table>';
            if (data.booking_btn) {
                html +='<div class="col-sm-12 text-center"><button type="submit" id="bookingbtn" class="btn btn-primary bookingbtn">Book Now</button></div>';
            }
            if (data.Total_pax ==0 || data.status==0) {
                html +='<div class="col-sm-12 col-xs-12" id="noPriceDiv"><b><br>Please select tour <u>Date</u>, <u>Type</u> and <u> Adults</u></b><br><br></div>';
            }
            console.log(data);
            $('#CalcResult').html(html);
        }
        });
    }


function getquerystring(formname) {
    var form = document.forms[formname];
	var qstr = "";
    function GetElemValue(name, value) {
        qstr += (qstr.length > 0 ? "&" : "")
            + escape(name).replace(/\+/g, "%2B") + "="
            + escape(value ? value : "").replace(/\+/g, "%2B");
    }
	var elemArray = form.elements;
    for (var i = 0; i < elemArray.length; i++) {
        var element = elemArray[i];
        var elemType = element.type.toUpperCase();
        var elemName = element.name;
        if (elemName) {
            if (elemType == "TEXT"
                    || elemType == "TEXTAREA"
                    || elemType == "PASSWORD"
					|| elemType == "BUTTON"
					|| elemType == "RESET"
					|| elemType == "SUBMIT"
					|| elemType == "FILE"
					|| elemType == "IMAGE"
                    || elemType == "HIDDEN")
                GetElemValue(elemName, element.value);
            else if (elemType == "CHECKBOX" && element.checked)
                GetElemValue(elemName, 
                    element.value ? element.value : "On");
            else if (elemType == "RADIO" && element.checked)
                GetElemValue(elemName, element.value);
            else if (elemType.indexOf("SELECT") != -1)
                for (var j = 0; j < element.options.length; j++) {
                    var option = element.options[j];
                    if (option.selected)
                        GetElemValue(elemName,
                            option.value ? option.value : option.text);
                }
        }
    }
    return qstr;
}
function updatepage(str,responsediv){
    document.getElementById(responsediv).innerHTML = str;
}

    
/*window.onload = function() {
    if (window.jQuery) {
        // jQuery is loaded
        console.log("Yeah!");
    } else {
        // jQuery is not loaded
        console.log("Doesn't Work");
    }
}
$().ready(function () {
    console.log('works');
})*/
$(function() {
    @if ($tour->tourInfo->Is_Daily_Tour != 1)
$('#pax').on('change',function() {
    if ($('#pax').val()!=0) {
        var html='<div class="row"><div class="col-sm-4 col-xs-12"><div class="form-group"><label>Single</label>';
            html+='<select class="form-control required" style="border:solid 1px #CCC" name="singlea" onChange="';
            html+="xmlhttpPost('{{ $calculation_href }}');";
            html+='return false;">';
            html+='<option value="0" >0</option>';
            for (let paxs = 1; paxs < 41; paxs++) {
                html+='<option value="'+paxs+'">'+paxs+'</option>';
            }
            html+='</select></div></div>';
            html+='<div class="col-sm-4 col-xs-12"><div class="form-group"><label>Double</label><select class="form-control required" style="border:solid 1px #CCC" name="doublea" onChange="';
                html+="xmlhttpPost('{{ $calculation_href }}');"
                html+=' return false;"><option value="0">0</option>';
                for (let paxs = 1; paxs < 41; paxs++) {
                html+='<option value="'+paxs+'">'+paxs+'</option>';
                }
                html+='</select></div></div>';
                html+='<div class="col-sm-4 col-xs-12"><div class="form-group"><label>Triple</label><select class="form-control required" style="border:solid 1px #CCC" name="triplea" onChange="';
                html+="xmlhttpPost('{{ $calculation_href }}');"
                html+=' return false;"><option value="0">0</option>';
                for (let paxs = 1; paxs < 41; paxs++) {
                html+='<option value="'+paxs+'">'+paxs+'</option>';
                }
                html+='</select></div></div></div>';
        $('#paxa').html(html);    
    }else{
        $('#paxa').html('');
    }
   
    
});
@endif
$('#children').on('change',function() {
    var html='<div class="row">';
    if ($('#children').val()!=0) {
        
        for (childrens = 1; childrens <= $('#children').val(); childrens++){
            html+='<div class="col-sm-6 col-xs-12"><div class="form-group"><label>Child Age '+childrens+' *</label><select class="form-control required" style="border:solid 1px #CCC" name="child[]" id="cocuks" onChange="';
            html+="xmlhttpPost('{{ $calculation_href }}');";
            html+=' return false;"><option value="0"> Select </option>';
            for (let c = 0; c < 12; c++) {
                html+='<option value="'+c+'">'+c+'</option>';   
            }
            html+='</select></div></div>';
        }
        html+='</div>';
        $('#child').html(html);    
    }else{
        $('#child').html('');
    }
})
});
</script>

@endsection