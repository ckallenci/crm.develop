<div class="row justify-content-center">
    <div class="col-md-12">           
        
            <div class="alert alert-success" style="display:none" id="basari"></div>

        <div id="deneme">
            <form id="gonderilenform">
                <div class="row">
                    <div class="col-sm-6">
                        {{ Form::pText2('name', null, trans('site.enquiry_form_name')) }}
                        <span class="form_hidden cls-name" style="display:none; color:red"></span>
                    </div>
                    <div class="col-sm-6">
                        {{ Form::pText2('last_name', null, trans('site.enquiry_form_lastname')) }}
                        <span class="form_hidden cls-last-name" style="display:none; color:red"></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        {{ Form::pMobile2('mobile', null, trans('site.enquiry_form_mobile')) }}
                        <span class="form_hidden cls-mobile" style="display:none; color:red"></span>
                    </div>
                    <div class="col-sm-6">
                        {{ Form::pEmail2('email', null, trans('site.enquiry_form_email')) }}
                        <span class="form_hidden cls-email" style="display:none; color:red"></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <?php
                            $countries = App\Models\Country::get()->pluck('name', 'id');
                        ?>
                        {{ Form::pSelect2('country_id', null, $countries, trans('site.enquiry_form_country'), 'country_id') }}
                        <span class="form_hidden cls-country" style="display:none; color:red"></span>
                    </div>
                    <div class="col-sm-6">
                            <?php if (old('country_id')) {
                                $states = App\Models\State::where('country_id',old('country_id'))->pluck('name','id');
                            }else{
                                $states = array();
                            } ?>
                        {{ Form::pSelect2('state_id', null, $states, trans('site.enquiry_form_state'), 'state_id') }}
                        <span class="form_hidden cls-state" style="display:none; color:red"></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <?php if (old('state_id')) {
                            $cities = App\Models\City::where('state_id',old('state_id'))->pluck('name','id');
                        }else{
                            $cities = array();
                        } ?>
                        {{ Form::pSelect2('city_id', null, $cities, trans('site.enquiry_form_city'), 'city_id') }}
                        <span class="form_hidden cls-city" style="display:none; color:red"></span>
                    </div>
                    <div class="col-sm-6">
                        {{ Form::pText2('group_size', null, trans('site.enquiry_form_group_size')) }}
                        <span class="form_hidden cls-group" style="display:none; color:red"></span>
                    </div>
                </div>
                <div class="form-group row mb-0">
                    <div class="col-md-12 text-right">
                        {{ Form::pTextarea2('message',  null, trans('site.enquiry_form_message')) }}
                        <span class="form_hidden cls-message" style="display:none; color:red"></span>
                        {{ Form::hidden('tur_id', $tour_id) }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        @if ($errors->has('g-recaptcha-response'))
                            <span class="help-block">
                                <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                            </span>
                        @endif
                        <span class="form_hidden cls-g-recaptcha-respons" style="display:none; color:red"></span>
                        {!! NoCaptcha::display() !!}
                        <br>
                    </div>
                </div>
                <div class="form-group row mb-0">
                    <div class="col-md-12 text-right">
                        <input id="buton" class="btn btn-primary" type="button" value="{{ trans('site.enquiry_form_submit') }}"/>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@push('scripts')

{!! NoCaptcha::renderJs() !!}
<script>
    
    $(document).ready(function(){
        $("#buton").on("click", function(){ 
            var gonderilenform = $("#gonderilenform").serialize();
                         
            $.ajax({
                url:'/enquiry', 
                type:'POST', 
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data:'_token='+$('meta[name="csrf-token"]').attr('content')+'&'+gonderilenform, 
                success:function(data){

                    if(!data.errors)
                    {
                        jQuery('.alert-success').show();
                		jQuery('.alert-success').append('<p>'+data.success+'</p>');
                        $("#deneme").hide();
                    }
                    else
                    {
                        jQuery.each(data.errors, function(key, value){
                		    jQuery('.alert-danger').show();
                		    jQuery('.alert-danger').append('<p>'+value+'</p>');
                	    });
                    } 
                },
                error:function(data){

                    jQuery('.alert-danger').hide();
                    jQuery('.alert-danger').html('');
                    jQuery('.form_hidden').hide();
                    jQuery('.form_hidden').html('');

                    console.log(data.responseJSON);
                    
                    if(data.responseJSON.errors.name)
                    {
                        jQuery('.cls-name').show();
                	    jQuery('.cls-name').append('<strong>'+data.responseJSON.errors.name+'</strong>');
                    }
                    if(data.responseJSON.errors.last_name)
                    {
                        jQuery('.cls-last-name').show();
                	    jQuery('.cls-last-name').append('<strong>'+data.responseJSON.errors.last_name+'</strong>');
                    }
                    if(data.responseJSON.errors.mobile)
                    {
                        jQuery('.cls-mobile').show();
                	    jQuery('.cls-mobile').append('<strong>'+data.responseJSON.errors.mobile+'</strong>');
                    }
                    if(data.responseJSON.errors.email)
                    {
                        jQuery('.cls-email').show();
                  	    jQuery('.cls-email').append('<strong>'+data.responseJSON.errors.email+'</strong>');
                    }
                    if(data.responseJSON.errors.country_id)
                    {
                        jQuery('.cls-country').show();
                  	    jQuery('.cls-country').append('<strong>'+data.responseJSON.errors.country_id+'</strong>');
                    }
                    if(data.responseJSON.errors.state_id)
                    {
                        jQuery('.cls-state').show();
                  	    jQuery('.cls-state').append('<strong>'+data.responseJSON.errors.state_id+'</strong>');
                    }
                    if(data.responseJSON.errors.city_id)
                    {
                        jQuery('.cls-city').show();
                  	    jQuery('.cls-city').append('<strong>'+data.responseJSON.errors.city_id+'</strong>');
                    }
                    if(data.responseJSON.errors.group_size)
                    {
                        jQuery('.cls-group').show();
                  	    jQuery('.cls-group').append('<strong>'+data.responseJSON.errors.group_size+'</strong>');
                    }
                        
                    if(data.responseJSON.errors.message)
                    {
                        jQuery('.cls-message').show();
                  		jQuery('.cls-message').append('<strong>'+data.responseJSON.errors.message+'</strong>');
                    } 
                    if(data.responseJSON.message)
                    {
                        jQuery('.cls-g-recaptcha-respons').show();
                  		jQuery('.cls-g-recaptcha-respons').append('<strong>'+data.responseJSON.message+'</strong>');
                    } 
                    /*jQuery.each(data.responseJSON.errors, function(key, value){
                        console.log(data.responseJSON.errors.name);
                  		jQuery('.alert-danger').show();
                  		jQuery'.alert-danger').append('<p>'+value+'</p>');
                  	});*/
                }
            });
        });         
    });
</script>


<script src="{{ asset('js/intl-tel-input/build/js/intlTelInput-jquery.js') }}"></script>
<script>
    $( document ).ready(function() {
        $('#country_id').on('change',function() {
            var country_id = $('#country_id').val();
            $.ajax({
                type: "GET",
                url: "{{ route('ajax.state') }}",
                data:'country_id='+country_id,
                success: function(data){
                    $('#state_id').empty();
                    $('#state_id').append($('<option>').text('{{__('form.register_state_select')}}').attr('value', ""));
                    $.each(data, function(i, value) {
                        $('#state_id').append($('<option>').text(value).attr('value', i));
                    });
                }
            });
        });
        $('#state_id').on('change',function() {
            var state_id = $('#state_id').val();
            $.ajax({
                type: "GET",
                url: "{{ route('ajax.city') }}",
                data:'state_id='+state_id,
                success: function(data){
                    $('#city_id').empty();
                    $('#city_id').append($('<option>').text('{{__('form.register_city_select')}}').attr('value', ''));
                    $.each(data, function(i, value) {
                        $('#city_id').append($('<option>').text(value).attr('value', i));
                    });
                 }
            });
        });
    });   
</script>
    
@endpush
