@if ($contentitem)
<div class="gallery_area">
    <div class="container-fluid">
        <div class="row mar-padding-15">
            @if ($contentitem->content_files)
                @foreach ($contentitem->content_files as $fileitem)
                    <div class="col-6 col-md-4 col-lg-3 item">
                        <div class="img">
                            <a class="venobox vbox-item" href="{{ asset($fileitem->file) }}"><img class="img-fluid" src="{{ asset($fileitem->file_thumbnail) }}" alt=""></a>
                        </div>
                    </div>
                @endforeach
            @endif
            
        </div>
    </div>
</div>
@endif
