@if ($contentitem)
<div class="row">
    @if ($contentitem->title!="")
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!--Start Heading Title-->
                    <div class="heading-title">
                            
                            <h2 class="f-weight-700 margin-0">{{ $contentitem->title }}</h2>
                            <div class="bordershep"></div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if ($contentitem->content!="")
        {!! string_search($contentitem->content) !!}
    @endif
</div>
@endif
