@if ($sponsors)
    <section class="referans_area">
		<div class="container">
			<div class="row">
                @foreach ($sponsors as $sponsoritem)
                    <div class="col-6 col-md text-center">
						<a href="{{ $sponsoritem->url }}" target="_blank"><img class="img-fluid" src="{{ $sponsoritem->image }}" alt="{{ $sponsoritem->name }}"></a>
					</div>
                @endforeach
            </div>
        </div>
    </div>
@endif