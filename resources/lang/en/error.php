<?php
    return [
        '404h1' => 'WE ARE SORRY',
        '404content' => 'We seem to have lost this page, try one of these instead',
    ];
?>