<?php

return [
    'form_input_browse' => 'Browse',

    'register_register'         => 'Register',
    'register_name'             => 'Full Name',
    'register_email'            => 'E-Mail Address',
    'register_password'             => 'Password',
    'register_confirm_password'     => 'Confirm Password',
    'register_agency_name'      => 'Agency Name',
    'register_job'              => 'Your Job Title/Position',
    'register_mobile'           => 'Mobile',
    'register_office_phone'     => 'Office Phone',
    'register_country'          => 'Country',
    'register_country_select'   => 'Select Country',
    'register_state'            => 'State',
    'register_state_select'     => 'Select State',
    'register_city'             => 'City',
    'register_city_select'      => 'Select City',
    'register_postal_code'      => 'Postal Code',
    'register_address'          => 'Address',
    'register_site'             => 'Web Site',
    'register_trade_association'        => 'Trade Association',
    'register_trade_association_no'     => 'Trade Assoc. MemberS. No',
    'register_btn_register'             => 'Register',

    'register_agency'   => 'Agency Register',
    'register_user'     => 'User Register',
    'login'             => 'Login',
    'remember_me'       => 'Remember Me',
    'forgot_password'   => 'Forgot Your Password?',
    
];