<?php

return [
    'form_input_browse' => 'Bul',

    'register_register'         => 'Üye Ol',
    'register_name'             => 'Ad Soyad',
    'register_email'            => 'E-Mail Adresi',
    'register_password'             => 'Şifre',
    'register_confirm_password'     => 'Şifre Tekrar',
    'register_agency_name'      => 'Acenta Adı',
    'register_job'              => 'Ünvan',
    'register_mobile'           => 'Telefon No',
    'register_office_phone'     => 'Ofis Telefonu No',
    'register_country'          => 'Ülke',
    'register_country_select'   => 'Ülke Seçiniz',
    'register_state'            => 'Şehir',
    'register_state_select'     => 'Şehir Seçiniz',
    'register_city'             => 'Semt',
    'register_city_select'      => 'Semt Seçiniz',
    'register_postal_code'      => 'Posta Kodu',
    'register_address'          => 'Adres',
    'register_site'             => 'Web Site',
    'register_trade_association'        => 'Ticaret Birliği',
    'register_trade_association_no'     => 'Ticaret Birliği Üye No',
    'register_btn_register'             => 'Kayıt Ol',

    'register_agency'   => 'Acenta Üyeliği',
    'register_user'   => 'Kullanıcı Üyeliği',
    'login' => 'Giriş Yap',
    'remember_me'   => 'Beni Hatırla',
    'forgot_password'   => 'Şifremi Unuttum',

];