<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lang_id');
            $table->integer('page_id');
            $table->string('title');
            $table->string('meta_desc');
            $table->string('meta_keyword');
            $table->string('redirect_url')->nullable();
            $table->integer('page_type_id');
            $table->integer('homepage');
            $table->integer('topmenu');
            $table->integer('submenu');
            $table->string('page_img')->nullable();
            $table->integer('status');
            $table->integer('order');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
