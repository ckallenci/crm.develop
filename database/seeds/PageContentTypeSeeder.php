<?php

use Illuminate\Database\Seeder;
use App\Models\PageContentType;

class PageContentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $pagecontenttypes = array(
            array('id' => 1, 'name' => "Metin", 'path' => 'contet', 'status' => 1),
            array('id' => 2, 'name' => "Galeri", 'path' => 'imagegallery-1', 'status' => 1),
            array('id' => 3, 'name' => "Map", 'path' => 'map', 'status' => 1),
            array('id' => 4, 'name' => "Tours Widget", 'path' => 'tour', 'status' => 1),
           
        );

        PageContentType::truncate();
        PageContentType::insert($pagecontenttypes);

    }
}
